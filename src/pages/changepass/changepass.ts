import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginPage } from '../../pages/login/login';
import { WebService } from '../../providers/webservice';

@Component({
  selector: 'page-changepass',
  templateUrl: 'changepass.html'
})

export class ChangepassPage {

  formChangepass: FormGroup;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public webservice: WebService
  ) {

  	this.formChangepass = this.formBuilder.group({
      oldpass: ['', Validators.required],
      newpass: ['', Validators.required],
      newpassrepeat: ['', Validators.required],
    });

  }


  submitChangepass() {
    let value = this.formChangepass.value;

    if(value.newpass != value.newpassrepeat){
      const alert = this.alertCtrl.create({
        message: 'La nueva contraseña no coincide con la contraseña de repetición',
        buttons: [{
          text: 'CERRAR',
          role: 'cancel'
        }]
      });
      alert.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      content: 'Actualizando su contraseña, espere...'
    });
    loading.present();

    setTimeout(() => {
      this.webservice.changePass(value.oldpass, value.newpass)
      .then(
        data => {

          loading.dismissAll();
          this.navCtrl.popToRoot();
          const alert = this.alertCtrl.create({
            title: 'Contraseña cambiada correctamente.'
          });
          alert.present();

        },
        err => {

          let msgerror = err.error ? err.error 
          	: 'Vaya... parece que tenemos problemas para actualizar tu contraseña. Inténtelo de nuevo más tarde.';

          loading.dismissAll();
          console.log("Error al cambiar contraseña: " + JSON.stringify(err));
          const alert = this.alertCtrl.create({
            message: msgerror,
            buttons: [{
              text: 'CERRAR',
              role: 'cancel'
            }]
          });
          alert.present();

        }
      );

    }, 300);
  };

}
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { WebService } from '../../providers/webservice';
import { LoginPage } from '../../pages/login/login';
import { UserService } from '../../providers/userservice';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})

export class RegisterPage {

  formRegister: FormGroup;
  public userTypes: any;
  private user: any;
  public regions: any;
  public provinces: any;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private webservice: WebService,
    private userService: UserService
  ) {
    console.log("RegisterPage");
    this.formRegister = this.formBuilder.group({
      name: ['', Validators.required],
      surnames: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      cpassword: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      province: ['', Validators.required],
      region: ['', Validators.required],
      postal_code: ['', Validators.required],
      type: [1, Validators.required] //para Particular o Tienda
    });

    this.userTypes = [
        {id:1, name: 'Particular'},
        {id:2, name: 'Profesional'}
    ];

    this.user = {};
    this.regions = [];
    this.provinces = [];
  }

  ngOnInit() {
        //Cargar comunidades y provincias:
        this.loadRegions();
        this.loadProvinces();
  };

  loadRegions() {
    this.webservice.getRegions().then(
      (data:any) => {
        this.regions = data;
      },
      (error:any) => {
      }
    );
  };

  loadProvinces() {
    this.webservice.getProvinces().then(
      (data:any) => {
        this.provinces = data;
      },
      (error:any) => {
      }
    );
  };

  //Filtrar provincias por comunidad seleccionada:
  filterProvinces() {
    if(this.formRegister.value.region){
      return this.provinces.filter((item) => {
        return item.comunidad_id == this.formRegister.value.region;
      });
    }
    else
      return this.provinces;
  };

  login() {
    this.navCtrl.push(LoginPage, {}, {
        animate: true,
        animation: 'transition',
        direction: 'back'
    }).then(() => {
        this.navCtrl.remove(this.navCtrl.getActive().index - 1);
    });
  };


  showError(errorMsg) {
    let alert = this.alertCtrl.create({
        title: 'Error de validación',
        subTitle: errorMsg,
        buttons: [{
            text: 'CERRAR',
            role: 'cancel'
        }]
    });
    alert.present();
  }

  validate(): boolean {
      let value = this.formRegister.value;
      let errorMsg = '';

      //Validar contraseñas:
      if(value.password !== value.cpassword){
        errorMsg = '¡Las contraseñas no coinciden!',
        this.showError(errorMsg);
        return false;
      }

      //Validar código postal:
      let cp = value.postal_code;
      let cpnumber = null;
      try{cpnumber = parseInt(cp);}
      catch(e){cpnumber = null;}
      if(!cpnumber || cp.length != 5){
          errorMsg = 'El código postal debe ser un número de 5 cifras';
          this.showError(errorMsg);
          return false;
      }

      return true;
  }

  submitRegister() {

    if (!this.validate()) {
      return;
    }

    let value = this.formRegister.value;
    
    const loading = this.loadingCtrl.create({
      content: 'Creando su cuenta, espere...'
    });
    loading.present();

    //Asignar valores que han podido cambiar
    this.user.name = value.name;
    this.user.surnames = value.surnames;
    this.user.email = value.email;
    this.user.password = value.password;
    this.user.phone = value.phone;
    this.user.address = value.address;
    this.user.city = value.city;
    this.user.province = value.province;
    this.user.region = value.region;
    this.user.postal_code = value.postal_code;
    this.user.type = value.type;
    this.user.reg_token = this.userService.getPushToken(); //localStorage.getItem('TokenIdRegistered');

    setTimeout(() => {
      this.webservice.register(this.user).then(
          (data:any) => {
            loading.dismissAll();
            this.userService.setUser(data);
            localStorage.setItem('UserId', data.id);
            //console.log("api_token: " + data.api_token);
            localStorage.setItem('api_token', data.api_token);
            this.navCtrl.popToRoot();
          },
          err => {
            loading.dismissAll();
            console.log("Error al registrar: " + JSON.stringify(err));
            const alert = this.alertCtrl.create({
              title: 'Atención...',
              message: '¡La dirección de email ya se encuentra registrada en nuestro sistema! Si es usted por favor, inicie sesión. Si ha olvidado su contraseña nosotros le ayudamos a recuperarla.',
              buttons: [{
                text: 'VOLVER A INTENTARLO',
                role: 'cancel'
              }]
            });
            alert.present();
          }
      );
    }, 1000); 
    
  };

}
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { Push } from '@ionic-native/push';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { ImageResizer } from '@ionic-native/image-resizer';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { Traelo } from './app.component';
import { HomePage } from '../pages/home/home';
import { PNSearchPage } from '../pages/pnsearch/pnsearch';
import { SearchPage } from '../pages/search/search';
import { AddProductPage } from '../pages/addProduct/addProduct';
import { ConversationsPage } from '../pages/conversations/conversations';
import { LoginPage } from '../pages/login/login';
import { RecoverPage } from '../pages/recover/recover';
import { RegisterPage } from '../pages/register/register';
import { DetailsPage } from '../pages/details/details';
import { ProfilePage } from '../pages/profile/profile';
import { ChatPage } from '../pages/chat/chat';
import { MyProfilePage } from '../pages/myprofile/myprofile';
import { DetailsMyProductPage } from '../pages/detailsmyproduct/detailsmyproduct';
import { EditProductPage } from '../pages/editproduct/editproduct';
import { EditMyProfilePage } from '../pages/editmyprofile/editmyprofile';
import { ResultsPage } from '../pages/results/results';
import { PurchasePage } from '../pages/purchase/purchase';
import { RatingUserPage } from '../pages/ratinguser/ratinguser';
import { ChangepassPage } from '../pages/changepass/changepass';
import { ContactPage } from '../pages/contact/contact';


import { WebService } from '../providers/webservice';
import { UserService } from '../providers/userservice';
import { TpvService } from '../providers/tpvservice';
import { ChatService } from '../providers/chatservice';

@NgModule({
  declarations: [
    Traelo,
    HomePage,
    PNSearchPage,
    SearchPage,
    AddProductPage,
    ConversationsPage,
    LoginPage,
    RecoverPage,
    RegisterPage,
    DetailsPage,
    ProfilePage,
    ChatPage,
    MyProfilePage,
    DetailsMyProductPage,
    EditProductPage,
    EditMyProfilePage,
    ResultsPage,
    PurchasePage,
    RatingUserPage,
    ChangepassPage,
    ContactPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(Traelo, {
      backButtonText: ''
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Traelo,
    HomePage,
    PNSearchPage,
    SearchPage,
    AddProductPage,
    ConversationsPage,
    LoginPage,
    RecoverPage,
    RegisterPage,
    DetailsPage,
    ProfilePage,
    ChatPage,
    MyProfilePage,
    DetailsMyProductPage,
    EditProductPage,
    EditMyProfilePage,
    ResultsPage,
    PurchasePage,
    RatingUserPage,
    ChangepassPage,
    ContactPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Push,
    Camera,
    FileTransfer,
    ImageResizer,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WebService,
    UserService,
    TpvService,
    ChatService
  ]
})
export class AppModule {}

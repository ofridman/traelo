import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
 
@Injectable()

export class UserService {
  public USER_LOGGED: string = 'userLogged';
  public API_TOKEN: string = 'api_token';
  public PUSH_TOKEN: string = 'reg_token';
  private subject = new Subject<any>();

  constructor() {
    var user = localStorage.getItem(this.USER_LOGGED) ? JSON.parse(localStorage.getItem(this.USER_LOGGED)) : null;
    if (user) {
      this.subject.next(user);
    } 
  }
  
  getUser(){
    var user = localStorage.getItem(this.USER_LOGGED) ? JSON.parse(localStorage.getItem(this.USER_LOGGED)) : null;
    return user;
  }

  setUser(user: any) {
    localStorage.setItem(this.USER_LOGGED, JSON.stringify(user));
    this.subject.next(user);
  }

  clearUser() {
    localStorage.removeItem(this.USER_LOGGED);
    localStorage.removeItem(this.API_TOKEN);
    this.subject.next();
  }

  getApiToken() {
    return localStorage.getItem(this.API_TOKEN);
  }

  getPushToken(){
    return localStorage.getItem(this.PUSH_TOKEN);
  }
  savePushToken(tokenId) {
    localStorage.setItem(this.PUSH_TOKEN, tokenId);
  }

  isUserLogged() {
    return !!localStorage.getItem(this.USER_LOGGED);
  }

  onChangeUser(): Observable<any> {
    return this.subject.asObservable();
  }
}


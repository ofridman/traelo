import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebService } from '../../providers/webservice';

import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';

import { UserService } from '../../providers/userservice';

import { ChangepassPage } from '../../pages/changepass/changepass';

@Component({
    selector: 'page-editmyprofile',
    templateUrl: 'editmyprofile.html'
})

export class EditMyProfilePage {

    @ViewChild(Content) content: Content;

    private urlUploadAvatars: string;
    private userid: any;
    public user: any;
    public userTypes: any;
    public formProfile: any;
    public provinces: any;
    public regions: any;

    public imageAvatar: any;
    isImage: boolean = false;

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        public actionSheetCtrl: ActionSheetController,
        public formBuilder: FormBuilder,
        private camera: Camera,
        private transfer: FileTransfer,
        private imageResizer: ImageResizer,
        private webservice: WebService,
        private userService: UserService
    ) {
        this.urlUploadAvatars = webservice.getUrlAvatarImages();
        this.imageAvatar = 'assets/icon/no-avatar.png';
        this.userid = this.navParams.get('userid');
        this.user = {};

        this.formProfile = this.formBuilder.group({
          name: ['', Validators.required],
          surnames: ['', Validators.required],
          email: ['', Validators.required],
          phone: ['', Validators.required],
          address: ['', Validators.required],
          city: ['', Validators.required],
          province: ['', Validators.required],
          region: ['', Validators.required],
          postal_code: ['', Validators.required],
          //postal_code: ['', Validators.compose([Validators.required, Validators.maxLength(5), Validators.minLength(5)])],
          type: [1, Validators.required], //para Particular o Tienda
          iban: ['']
        });

        this.userTypes = [
            {id:1, name: 'Particular'},
            {id:2, name: 'Profesional'}
        ];
        this.regions = [];
        this.provinces = [];
    }

    ngOnInit() {
        //Cargar datos del usuario:
        this.loadUserData();
        //Cargar comunidades y provincias:
        this.loadRegions();
        this.loadProvinces();
    };

    loadUserData() {
        setTimeout(() => {
          if(this.userid){
            this.webservice.getMyUser(this.userid).then((data) => {
              //console.log("Cargar mis datos: " + JSON.stringify(data));
              if(data){
                this.user = data;
                if(this.user.avatar)
                  this.imageAvatar = this.urlUploadAvatars + '/' + this.user.id + '/' + this.user.avatar; 

                this.formProfile.get('name').setValue(this.user.name);
                this.formProfile.get('surnames').setValue(this.user.surnames);
                this.formProfile.get('email').setValue(this.user.email);
                this.formProfile.get('phone').setValue(this.user.phone);
                this.formProfile.get('address').setValue(this.user.address);
                this.formProfile.get('city').setValue(this.user.city);
                this.formProfile.get('postal_code').setValue(this.user.postal_code);
                this.formProfile.get('province').setValue(this.user.province);
                this.formProfile.get('region').setValue(this.user.region);
                this.formProfile.get('type').setValue(this.user.type);
                this.formProfile.get('iban').setValue(this.user.iban);
              }
            });//fin obtener mis datos
          }
        }, 200);
    };

    loadRegions() {
        this.webservice.getRegions().then(
          (data:any) => {
            this.regions = data;
          },
          (error:any) => {
          }
        );
    };

    loadProvinces() {
        this.webservice.getProvinces().then(
          (data:any) => {
            this.provinces = data;
          },
          (error:any) => {
          }
        );
    };

    //Filtrar provincias por comunidad seleccionada:
    filterProvinces() {
        if(this.formProfile.value.region){
          return this.provinces.filter((item) => {
            return item.comunidad_id == this.formProfile.value.region;
          });
        }
        else
          return this.provinces;
    };

    addAvatar() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Cambiar imagen avatar',
            buttons: [{
                text: 'HACER FOTO',
                icon: 'camera',
                handler: () => {
                    this.camera.getPicture({
                        destinationType: this.camera.DestinationType.FILE_URI,
                        encodingType: this.camera.EncodingType.JPEG,
                        sourceType: this.camera.PictureSourceType.CAMERA,
                        allowEdit: false,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    }).then((cameraUri) => {

                        let optionsResizerCamera = {
                            uri: cameraUri,
                            quality: 90,
                            width: 800,
                            height: 450
                        } as ImageResizerOptions;

                        this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                            this.isImage = true;
                            this.imageAvatar = cameraImage;
                        });
                        
                    });
                }
            }, {
                text: 'GALERÍA',
                icon: 'image',
                handler: () => {
                    this.camera.getPicture({
                        destinationType: this.camera.DestinationType.FILE_URI,
                        encodingType: this.camera.EncodingType.JPEG,
                        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                        allowEdit: false,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    }).then((galleryUri) => {

                        let optionsResizerGallery = {
                            uri: galleryUri,
                            quality: 90,
                            width: 800,
                            height: 450
                        } as ImageResizerOptions;
                        
                        this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                            this.isImage = true;
                            this.imageAvatar = galleryImage;
                        });
                        
                    });
                }
            }, {
                text: 'CANCELAR',
                icon: 'close',
                role: 'cancel'
            }]
        });
        actionSheet.present();
    };


    validate(): boolean {
        let value = this.formProfile.value;
        let errorMsg = '';

        //Validar código postal:
        let cp = value.postal_code;
        let cpnumber = null;
        try{cpnumber = parseInt(cp);}
        catch(e){cpnumber = null;}
        if(!cpnumber || cp.length != 5){
            errorMsg = 'El código postal debe ser un número de 5 cifras';
        }

        if(!errorMsg || errorMsg.length == 0)
            return true;

        let alert = this.alertCtrl.create({
            title: 'Error de validación',
            subTitle: errorMsg,
            buttons: [{
                text: 'CERRAR',
                role: 'cancel'
            }]
        });
        alert.present();

        return false;
    }

    submitProfile() {

        if (!this.validate()) {
          return;
        }

        let value = this.formProfile.value;
        //Asignar valores que han podido cambiar
        this.user.name = value.name;
        this.user.surnames = value.surnames;
        this.user.email = value.email;
        this.user.phone = value.phone;
        this.user.address = value.address;
        this.user.city = value.city;
        this.user.postal_code = value.postal_code;
        this.user.region = value.region;
        this.user.province = value.province;
        this.user.type = value.type;
        this.user.iban = value.iban;

        if(this.isImage){
            this.user.avatar = 'avatar-' + Date.now() +'.jpg';
        }

        let loading = this.loadingCtrl.create({
            content: 'Actualizando perfil, espere...'
        });
        loading.present();

        setTimeout(() => {

            this.webservice.updateMyUser(this.user)
            .then((dataUpdate: any) => {
    
                console.log("WOOOO update ok id: " + dataUpdate.id);
                this.userService.setUser(dataUpdate);

                //Subir imágen:
                this.uploadImage(this.user)
                .then(data => {
                    console.log("Resultado actualizar avatar OK: " + JSON.stringify(data));

                    loading.dismiss();
                    let alert = this.alertCtrl.create({
                        title: 'Tus datos se han actualizado',
                        buttons: [{
                            text: 'CERRAR',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }]
                    });
                    alert.present();

                }, (error) => {
                    console.log("Error al subir imagen de avatar: " + JSON.stringify(error));
                    loading.dismiss();
                    let alert = this.alertCtrl.create({
                        title: 'Oh oh...',
                        subTitle: 'Parece que ha fallado la transferencia de la imagen del avatar',
                        buttons: [{
                            text: 'CERRAR',
                            role: 'cancel'
                        }]
                    });
                    alert.present();
                    return;
                });
                
            }, (err: any) => {//Error al guardar
                
                let msgError = err.msgerror ? err.msgerror:'Parece que no se ha podido actualizar el perfil. Inténtelo de nuevo más tarde';
                loading.dismiss();
                let alert = this.alertCtrl.create({
                      title: 'Oh oh...',
                      subTitle: msgError,
                      buttons: [{
                          text: 'ENTENDIDO',
                          role: 'cancel'
                      }]
                });
                alert.present();
                return;
            });//Fin guardar

        }, 1000);
    };

    //Subir imágen de avatar
    uploadImage(user) {
        let uid: string = user.id;
        let uploadEdit = this.webservice.getUrlUpload() + 'uptype=changeAvatar&uid=' + uid;

        //Transferir imagen de avatar
        let promise1 = new Promise((resolve, reject) => {
            if(!this.isImage)
                return resolve(0);

            let url1 = uploadEdit + '&filename=' + user.avatar;
            const transferImage1: FileTransferObject = this.transfer.create();
            let optionsTransferImage1: FileUploadOptions = {
                fileKey: 'newAvatar',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage1.upload(this.imageAvatar, encodeURI(url1), optionsTransferImage1)
            .then((data) => {
                console.log("Upload img 1 ok: " + JSON.stringify(data));
                return resolve(1);
            }, (error) => {
                console.log("Error al subir imagen 1: " + JSON.stringify(error));
                return reject(error);
            });
        });

        return promise1;
    };

    changePass() {
        this.navCtrl.push(ChangepassPage);
    };

}
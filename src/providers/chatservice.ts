import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { WebService } from '../providers/webservice';
 
@Injectable()

//Esta clase se encarga de almacenar la lista de nuevos mensajes de chat recibidos, es decir, los no leídos.
export class ChatService {
  public MESSAGES_NAME: string = 'unread_messages';
  private messages: any;
  private subject = new Subject<any>();

  constructor(
    public webservice: WebService
  ) {
    this.messages = localStorage.getItem(this.MESSAGES_NAME) ? JSON.parse(localStorage.getItem(this.MESSAGES_NAME)) : [];
    this.subject.next(this.messages);
  }
  
  addMessage(message: any){
    //Comprobar si ya existe...
    let found = this.messages.find((item) => {
      return item.id == message.id;
    });
    if(found) return;

    //Añadir y guardar la lista
    this.messages.push(message);
    localStorage.setItem(this.MESSAGES_NAME, JSON.stringify(this.messages));
    this.subject.next(this.messages);
  }

  getAllMessages(){
    return this.messages;
  }

  getMessages(idConversation: any){
    let sublist = this.messages.filter((item) => {
        return item.conversation_id == idConversation;
    });
    return sublist;
  }

  //Descarga del servidor los mensajes no leídos:
  getUnreadMessages(iduser) {
    this.webservice.unreadMessages(iduser).then((data) => {
      this.messages = data;
      localStorage.setItem(this.MESSAGES_NAME, JSON.stringify(this.messages));
      this.subject.next(this.messages);
    });
  }

  clearMessages(idConversation: any) {
    console.log("clearMessages");
    //Eliminar los mensajes de una conversación:
    for( var i=this.messages.length - 1; i>=0; i--){
      if(this.messages[i] && (this.messages[i].conversation_id == idConversation)){
        console.log("message con id a eliminar: " + this.messages[i].id);
        this.messages.splice(i, 1);
      }
    }
    //Marcar como leídos en el servidor:
    this.webservice.readMessagesConversation(idConversation);

    localStorage.setItem(this.MESSAGES_NAME, JSON.stringify(this.messages));
    this.subject.next(this.messages);
  }

  onChangeMessages(): Observable<any> {
    return this.subject.asObservable();
  }
}


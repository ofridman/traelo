import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';

import { EditMyProfilePage } from '../../pages/editmyprofile/editmyprofile';
import { DetailsMyProductPage } from '../../pages/detailsmyproduct/detailsmyproduct';
import { WebService } from '../../providers/webservice';

import { UserService } from '../../providers/userservice';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'page-myprofile',
    templateUrl: 'myprofile.html'
})

export class MyProfilePage {

    @ViewChild(Content) content: Content;

    public username: string;
    public city: string;
    public productsList: any;
    private urlUploadImages: string;
    private urlUploadAvatars: string;
    private userid: any;
    public user: any;
    userAvatar: string;
    segmentUserProfile: string;
    contentFullscreen: string = 'true';
    subscription: Subscription;

    public valorations: any;

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        private webservice: WebService,
        private userService: UserService
    ) {
        this.segmentUserProfile = 'sales';

        this.urlUploadImages = webservice.getUrlProductImages();
        this.urlUploadAvatars = webservice.getUrlAvatarImages(); //'http://traelo.es/_WebService/_webservice/upload/avatars';
        this.userAvatar = 'assets/icon/no-avatar.png';
        this.userid = this.navParams.get('userid');
        this.user = {
            products: []
        };

        //subscribir a los cambios del usuario logado (para cuando hace actualización de datos de perfil)
        this.subscription = this.userService.onChangeUser().subscribe(user => { 
          //El segundo parametro no actualiza los productos
          this.refreshUser(user, false);
        });

        //TODO: coger de user.valorations:
        this.valorations = [];
    }

    ngOnInit() {
        //Cargar datos del usuario:
        this.loadUserData(this.userid);

        this.loadValorations(this.userid);
    };

    loadUserData(userid) {
        if(!userid) return null;
        return new Promise((resolve) => {
            setTimeout(() => {
                this.webservice.getUser(userid).then((data) => {
                  //console.log("Cargar mis datos: " + JSON.stringify(data));
                  if(data){
                    this.refreshUser(data, true);
                  }
                  resolve(data);
                });//fin obtener mis datos
            }, 200);
        });
    };

    //Actualiza los datos del usuario logado
    refreshUser(user, updateProducts) {
        if(!user){
            this.user = user;
            this.username = '';
            this.userAvatar = 'assets/icon/no-avatar.png';
            this.city = '';
        }else{
            //Antes de asignar, hacemos una copia de la lista de productos para que no se pierda
            let products = this.user ? JSON.parse(JSON.stringify(this.user.products)) : [];
            this.user = user;
            if(!updateProducts){
                this.user.products = products;
            }

            this.username = user.name + ' ' + user.surnames;
            this.userAvatar = user.avatar ? this.urlUploadAvatars + '/' + user.id + '/' + user.avatar : 'assets/icon/no-avatar.png'; 
            this.city = user.city;
        }
    };

    loadValorations(userid) {
        if(!userid) return null;

        return new Promise((resolve) => {
            setTimeout(() => {
                this.webservice.getValorations().then((data) => {
                  //console.log("Cargar mis datos: " + JSON.stringify(data));
                  if(data){
                    this.valorations = data;
                  }
                  resolve(data);
                });//fin obtener mis datos
            }, 200);
        });
    };

    viewDetails(id) {
        this.navCtrl.push(DetailsMyProductPage, {
          idproduct: id,
          parentPage: this
        });
    };

    editMyProfile() {
        this.navCtrl.push(EditMyProfilePage, {
          userid: this.userid
        });
    };

    getProductsNoSold(){
        if(!this.user || !this.user.products)
            return [];

        return this.user.products.filter((item) => {
            return item.sold == 'no';
        });
    }

    getProductsSold(){
        if(!this.user || !this.user.products)
            return [];

        return this.user.products.filter((item) => {
            return item.sold == 'yes';
        });
    }

    getImgProduct(product){
        if(!product.img1) 
            return 'assets/img/nophoto.png';
        return this.urlUploadImages+'/'+product.id+'/'+product.img1;
    }

    
    //Valoraciones
    countPositives(){
        if(!this.valorations)
            return 0;

        let votes = this.valorations.filter((item) => {
            return item.rating > 0;
        });
        return votes.length;
    };
    countNegatives(){
        if(!this.valorations)
            return 0;

        let votes = this.valorations.filter((item) => {
            return item.rating < 0;
        });
        return votes.length;
    };
    getAction(valoration){
        let action = valoration.action == 'C' ? 'Le compraste ' : 'Le vendiste ';
        return action + valoration.product.title;
    };

    //Hay que renovar?
    isRenew(product){
        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var date = product.updated_at ? new Date(product.updated_at) :
            product.created_at ? new Date(product.created_at) : null;
        if(!date) return false;

        var updated = new Date(date);
        var today = new Date();

        var diffDays = Math.round(Math.abs((today.getTime() - updated.getTime())/(oneDay)));
        return diffDays >= 30;
    };
    

    onScroll(event) {
        this.contentFullscreen = 'false';
    };

}
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebService } from '../../providers/webservice';
import { ResultsPage } from '../../pages/results/results';

@Component({
  selector: 'page-pnsearch',
  templateUrl: 'pnsearch.html'
})

export class PNSearchPage {

  formSearch: FormGroup;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private webservice: WebService,
  ) { 

    this.formSearch = this.formBuilder.group({
      searchText: ['']
    });

  }

  submitSearch() {

    let value = this.formSearch.value;
    if(!value.searchText){
      const alert = this.alertCtrl.create({
        title: 'Atención...',
        message: 'No ha introducido ningún criterio de búsqueda',
        buttons: [{
          text: 'ENTENDIDO',
          role: 'cancel'
        }]
      });
      alert.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      content: 'Buscando, espere...'
    });
    loading.present();
    
    let searchData = {
      codenumber: value.searchText
    };

    setTimeout(() => {
      this.webservice.productSearch(searchData).then(
          (data:any) => {
            loading.dismissAll();
            console.log("Resultados buscar: " + JSON.stringify(data));
            this.navCtrl.push(ResultsPage, {
              productsList: data
            });
          },
          err => {
            loading.dismissAll();
            console.log("Error al buscar: " + JSON.stringify(err));
            const alert = this.alertCtrl.create({
              title: 'Vaya...',
              message: 'No se ha podido completar la búsqueda...',
              buttons: [{
                text: 'VOLVER A INTENTARLO',
                role: 'cancel'
              }]
            });
            alert.present();
          }
      );
    }, 1000); 
    
  };

  back() {
    this.navCtrl.popToRoot();
  };

}
import { Component } from '@angular/core';
import { Platform, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { WebService } from '../../providers/webservice';
import { UserService } from '../../providers/userservice';
import { AddProductPage } from '../../pages/addProduct/addProduct';
import { ProfilePage } from '../../pages/profile/profile';
import { ChatPage } from '../../pages/chat/chat';
import { PurchasePage } from '../../pages/purchase/purchase';
import { LoginPage } from '../../pages/login/login';

@Component({
  selector: 'page-details',
  templateUrl: 'details.html'
})

export class DetailsPage {

  private urlUploadImages: string;
  private urlUploadAvatars: string;
  private idproduct: any;
  private iduser: any;
  private product: any;

  productCodeNumber: string;
  productIdUser: any;
  productUserTokenId: string;
  productUserName: string;
  productUserSurnames: string;
  productUserFirstLetter: string;
  productUserCity: string;
  productUserOnline: string;
  productUserAvatar: string;
  productTitle: string;
  productDescription: string;
  productKg: number;
  productPrice: string;
  productSure: string;
  productSold: string;
  productIsTraelo: string;
  productViews: number;
  productDateAdd: string;
  productImageOne: string;
  productImageTwo: string;
  productImageThree: string;
  productImageFour: string;
  productImageFive: string;
  productImageSix: string;
  defaultOfferInput: string;

  isLoading: boolean = true;
  isShowSlides: boolean = false;

  constructor(
    platform: Platform,
    public navCtrl: NavController,
    private navParams: NavParams,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private webservice: WebService,
    private userService: UserService
  ) {

    this.urlUploadImages = webservice.getUrlProductImages(); //'http://traelo.es/_WebService/upload/products';
    this.urlUploadAvatars = webservice.getUrlAvatarImages(); //'http://traelo.es/_WebService/upload/avatars';
    this.iduser = localStorage.getItem('UserId');
    this.idproduct = this.navParams.get('idproduct');

    const NO_PHOTO = 'assets/img/nophoto.png';
    
    setTimeout(() => {
      this.webservice.getProductDetails(this.idproduct)
      .then((data: any) => {

        if(!data || !data.user){
          let alert = this.alertCtrl.create({
            subTitle: 'Parece que este producto ya no se encuentra...',
            buttons: [{
              text: 'CERRAR',
              role: 'cancel'
            }],
            enableBackdropDismiss: false,
          });
          alert.present();
          return;
        }

        this.product = data;

        console.log("Pantalla detalle, producto: " + data.title);
        this.productCodeNumber = data.codenumber;
        this.productTitle = data.title;
        this.productDescription = data.description;
        this.productKg = data.kg;
        this.productPrice = data.price;
        this.productSure = data.sure;
        this.productSold = data.sold;
        this.productIsTraelo = data.istraelo;
        this.productViews = data.views;
        this.productDateAdd = data.created_at;
        this.productIdUser = data.user_id;
        this.productUserTokenId = data.usertokenid;
        if(data.user){
          this.productUserName = data.user.name;
          this.productUserSurnames = data.user.surnames;
          this.productUserFirstLetter = data.user.surnames ? data.user.surnames.substr(1, 1) : '';
          this.productUserCity = data.user.city;
          this.productUserOnline = data.user.online;
          if(data.user.avatar){
            this.productUserAvatar = this.urlUploadAvatars + '/' + this.productIdUser + '/' + data.user.avatar;
          }else{
            this.productUserAvatar = 'assets/icon/no-avatar.png';
          }
        }

        //Imagenes:
        if(data.img1){
          this.productImageOne = this.urlUploadImages + '/' + data.id + '/' + data.img1;
        }else{
          this.productImageOne = NO_PHOTO;
        }
        if(data.img2){
          this.productImageTwo = this.urlUploadImages + '/' + data.id + '/' + data.img2;
        }else{
          this.productImageTwo = NO_PHOTO;
        }
        if(data.img3){
          this.productImageThree = this.urlUploadImages + '/' + data.id + '/' + data.img3;
        }else{
          this.productImageThree = NO_PHOTO;
        }
        if(data.img4){
          this.productImageFour = this.urlUploadImages + '/' + data.id + '/' + data.img4;
        }else{
          this.productImageFour = NO_PHOTO;
        }
        if(data.img5){
          this.productImageFive = this.urlUploadImages + '/' + data.id + '/' + data.img5;
        }else{
          this.productImageFive = NO_PHOTO;
        }
        if(data.img6){
          this.productImageSix = this.urlUploadImages + '/' + data.id + '/' + data.img6;
        }else{
          this.productImageSix = NO_PHOTO;
        }

        this.defaultOfferInput = this.productPrice;
        this.isLoading = false;
      });
    }, 100);
  }

  showSlides() {
    if(this.productImageTwo || this.productImageThree || this.productImageFour || this.productImageFive || this.productImageSix){
      this.isShowSlides = true;
    }
  };

  closeSlides() {
    this.isShowSlides = false;
  };

  sendOffer() {
    if(this.iduser === this.productIdUser){
      let alert = this.alertCtrl.create({
        subTitle: '¡No puedes enviar ofertas a tus propios productos!',
        buttons: [{
          text: 'CERRAR',
          role: 'cancel'
        }],
        enableBackdropDismiss: false,
      });
      alert.present();
    }else{
      let alert = this.alertCtrl.create({
        title: 'Enviar oferta',
        inputs: [{
            name: 'amount',
            value: '' + this.defaultOfferInput + '',
            type: 'number'
        }],
        buttons: [{
            text: 'CANCELAR',
            role: 'cancel'
        }, {
            text: 'ENVIAR',
            handler: dataOffer => {
              if(dataOffer.amount !== ''){
                const loading = this.loadingCtrl.create();
                loading.present();
                setTimeout(() => {
                  this.webservice.sendOffer(this.idproduct, this.iduser, this.productIdUser, dataOffer.amount).then((data: any) => {
                    if(data.send){
                      let alert = this.alertCtrl.create({
                        title: '¡No se ha podido enviar la oferta!',
                        subTitle: 'No ha sido posible enviar una oferta a este producto ya que solo se permite enviar una oferta por producto.',
                        buttons: [{
                          text: 'CERRAR',
                          role: 'cancel'
                        }],
                        enableBackdropDismiss: false
                      });
                      alert.present();
                      loading.dismissAll();
                    }else if(data.insert){
                      this.webservice.sendPush(
                        'offer', 
                        this.productTitle, 
                        'Alguien está interesado en el producto "' + this.productTitle + '" y le ha ofrecido ' + dataOffer.amount + '€', 
                        this.productUserTokenId
                      ).then((data: any) => {
                        if(data.send){
                          let alert = this.alertCtrl.create({
                            title: '¡Oferta enviada!',
                            subTitle: 'Recibirás un aviso cuando el propietario del producto acepte o rechace tu oferta.',
                            buttons: [{
                              text: 'CERRAR',
                              role: 'cancel'
                            }],
                            enableBackdropDismiss: false,
                          });
                          alert.present();
                          loading.dismissAll();
                        }
                      });
                    }
                  });
                }, 1000);
              }
            }
        }],
        enableBackdropDismiss: false,
        cssClass: 'alertDefault'
      });
      alert.present();
    }
  };

  infoSecurePay() {
    let alert = this.alertCtrl.create({
      title: 'Pago seguro',
      subTitle: 'Pago fácil y seguro a través del Cyberpac de CaixaBank.',
      cssClass: 'alertInfo'
    });
    alert.present();
  };

  infoDelivery() {
    let alert = this.alertCtrl.create({
      title: 'Entrega en casa',
      subTitle: 'La compañía de transporte urgente MRW recoge el artículo y se lo lleva a su domicilio al día siguiente.',
      cssClass: 'alertInfo'
    });
    alert.present();
  };

  infoGuarantee() {
    let alert = this.alertCtrl.create({
      title: 'Garantía de devolución',
      subTitle: 'Garantía de devolución del artículo con portes gratis si éste no se ajusta a la descripción del vendedor.',
      cssClass: 'alertInfo'
    });
    alert.present();
  };

  viewProfile(userId) {
    this.navCtrl.push(ProfilePage, {
      userid: userId
    });
  }

  startChat() {
    if(!this.checkLoggedUser()) return;

    this.navCtrl.push(ChatPage, {
        product: this.product,
        userid: this.iduser
    });
  };

  purchase() {
    if(!this.checkLoggedUser()) return;

    this.navCtrl.push(PurchasePage, {
        product: this.product
    });
  };

  checkLoggedUser(){
    if(this.userService.isUserLogged()) return true;

    let alert = this.alertCtrl.create({
      title: 'Necesita logarse',
      subTitle: 'Esta acción requiere iniciar sesión. Por favor, inicie sesión o regístrese',
      cssClass: 'alertInfo'
    });
    alert.present();

    this.navCtrl.push(LoginPage, {
      sendPage: 'home'
    });
    return false;
  };


}
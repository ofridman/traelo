import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

import { WebService } from '../../providers/webservice';
import { UserService } from '../../providers/userservice';
import { ChatService } from '../../providers/chatservice';
import { SearchPage } from '../../pages/search/search';
import { PNSearchPage } from '../../pages/pnsearch/pnsearch';
import { AddProductPage } from '../../pages/addProduct/addProduct';
import { ConversationsPage } from '../../pages/conversations/conversations';
import { LoginPage } from '../../pages/login/login';
import { DetailsPage } from '../../pages/details/details';
import { DetailsMyProductPage } from '../../pages/detailsmyproduct/detailsmyproduct';
import { MyProfilePage } from '../../pages/myprofile/myprofile';
import { RatingUserPage } from '../../pages/ratinguser/ratinguser';
import { ContactPage } from '../../pages/contact/contact';

import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  public username: string;
  public productsList: any;

  private urlUploadImages: string;
  private urlUploadAvatars: string;
  //private userid: any;
  userLogged: any;
  subscription: Subscription;
  message: any;

  userAvatar: string;
  isLogged: boolean = false;
  isOnline: boolean = false;
  moreScroll: number = 10;

  isLoading: boolean = true;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    private webservice: WebService,
    private userService: UserService,
    private chatservice: ChatService,
    private inAppBrowser: InAppBrowser
  ) {
    this.urlUploadImages = webservice.getUrlProductImages();
    this.urlUploadAvatars = webservice.getUrlAvatarImages(); //'http://traelo.es/_WebService/_webservice/upload/avatars';
    
    this.userLogged = this.userService.getUser();
    if(this.userLogged) 
      console.log("Home - Hay usuario logado con id: " + this.userLogged.id);
    
    this.refreshUser(this.userLogged);

    //subscribir a los cambios del usuario logado (para cuando hace logout, login, registro, etc)
    this.subscription = this.userService.onChangeUser().subscribe(user => { 
      //console.log("Home - user cambia: " + JSON.stringify(user));
      this.refreshUser(user);
    });

  };

  ngOnDestroy() {
    //unsubscribe
    this.subscription.unsubscribe();
  };

  ngOnInit() {
    //Cargar datos del usuario logado:
    this.loadUserData();
    //Cargar productos del muro
    this.loadProductsList(0);

    if(this.userLogged && this.userLogged.id){
      //Comprobar si tiene mensajes no leídos:
      this.loadUnreadMessages(this.userLogged.id);

      //Comprobar si el usuario tiene valoraciones pendientes:
      this.loadPendingValorations(this.userLogged.id);
    }
      
  };

  //Carga la lista principal de productos:
  loadProductsList(refresher) {
    setTimeout(() => {
      this.webservice.getProductsList().then((data) => {
        this.productsList = data;
        this.isLoading = false;
        //Decimos al pull to refresh que ya ha terminado de refrescar:
        if(refresher != 0){
          refresher.complete();
        }
      });
    }, 1000);
  };

  //Si hay un pedido pendiente de valorar, mostramos la pantalla de valorar:
  loadPendingValorations(userid) {
    setTimeout(() => {
      this.webservice.getPendingValorations(userid).then((data) => {
        if(data && data[0]){
          this.ratingUser(data[0]);
        }
      });
    }, 1200);
  };

  //Si tiene mensajes sin leer:
  loadUnreadMessages(iduser) {
    setTimeout(() => {
      this.chatservice.getUnreadMessages(iduser);
    }, 1400);
  };
         

  //Sincroniza los datos del usuario logado cogiéndolos del servidor
  loadUserData() {
    setTimeout(() => {
      if(this.userLogged){
        this.webservice.getMyUser(this.userLogged.id).then((dataUser) => { //TODO: cargar datos por reg_token en lugar de por id...
          if(dataUser){
            this.refreshUser(dataUser);
          }
        });//fin obtener datos usuario logado del servidor
      }
    }, 2000);
  };

  //Actualiza los datos del usuario logado
  refreshUser(user) {
    this.userLogged = user;
    if(!user){
      this.username = '';
      this.userAvatar = 'assets/icon/no-avatar.png';
      this.isLogged = false;
      this.isOnline = false;
    }else{
      this.username = user.name + ' ' + user.surnames;
      this.userAvatar = user.avatar ? this.urlUploadAvatars + '/' + user.id + '/' + user.avatar : 'assets/icon/no-avatar.png'; 
      this.isLogged = true;
      this.isOnline = true;
    }
  };

  search() {
    this.navCtrl.push(SearchPage);
  };

  addProduct() {
    if(!this.checkLoggedUser(true)) return;

    this.navCtrl.push(AddProductPage);
  };

  viewDetails(product) {
    //Si es producto del usuario, ver página de modificación:
    if(this.userLogged && this.userLogged.id == product.user_id){
      this.navCtrl.push(DetailsMyProductPage, {
          idproduct: product.id,
          parentPage: this
      });
      return;
    }
    this.navCtrl.push(DetailsPage, {
      idproduct: product.id
    });
  };

  conversations() {
    if(!this.checkLoggedUser(true)) return;

    this.navCtrl.push(ConversationsPage, {
      userid: this.userLogged ? this.userLogged.id : null
    });
  };

  logout() {
    const loading = this.loadingCtrl.create({
      content: 'Desconectando, espere...'
    });
    loading.present();
    setTimeout(() => {
      this.userService.clearUser();
      this.navCtrl.popToRoot();
      loading.dismissAll();
    }, 1000);
  };

  myProfile() {
    if(!this.checkLoggedUser(false)) return;

    this.navCtrl.push(MyProfilePage, {
      userid: this.userLogged.id
    });
  };

  openPNSearch() {
    this.navCtrl.push(PNSearchPage);
  };

  openHelp() {
    let browser = this.inAppBrowser.create("http://traelo.es/pages/como_funciona.html",'_blank', {location: 'no'});
  };

  openContact() {
    this.navCtrl.push(ContactPage);
  };

  ratingUser(valoration) {
    this.navCtrl.push(RatingUserPage, {
      valoration: valoration
    });
  };

  unreadMessages() {
    return this.chatservice.getAllMessages().length;
  }

  checkLoggedUser(mostrarAlert){
    if(this.userService.isUserLogged()) return true;

    if(mostrarAlert!==false){
      let alert = this.alertCtrl.create({
        title: 'Necesita logarse',
        subTitle: 'Esta acción requiere iniciar sesión. Por favor, inicie sesión o regístrese',
        cssClass: 'alertInfo'
      });
      alert.present();
    }

    this.navCtrl.push(LoginPage, {
      sendPage: 'home'
    });
    return false;
  };

}
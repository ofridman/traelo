import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebService } from '../../providers/webservice';
import { TpvService } from '../../providers/tpvservice';

@Component({
  selector: 'page-purchase',
  templateUrl: 'purchase.html'
})

export class PurchasePage {

  //formPay: FormGroup;
  private user: any;
  private product: any;
  private shippingCosts: any;
  private total: any;

  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private webservice: WebService,
    private tpvservice: TpvService
  ) {

    this.product = this.navParams.get('product');
    this.shippingCosts = 0;
    this.total = this.product.price;


  }

  ngOnInit() {
        //Cargar gastos de envío:
        this.loadShippingCosts();

        //TODO: antes de iniciar pago, comprobar si producto ya fue vendido
  };

  loadShippingCosts() {
    this.webservice.getShippingCosts(this.product.id).then(
      (data:any) => {

        if(!data.shippingcosts || data.shippingcosts=='-1'){
          this.shippingCosts = 'Error';
          const alert = this.alertCtrl.create({
            title: 'Vaya...',
            message: 'No se han podido calcular los gastos de envío',
            buttons: [{
              text: 'ENTENDIDO',
              role: 'cancel'
            }]
          });
          alert.present();
          return;
        }

        this.shippingCosts = Number(data.shippingcosts);
        this.total = Number(this.total) + this.shippingCosts;
      },
      (error:any) => {
        console.log("Error al obtener gastos de envío");
      }
    );
  };

  tpvCallback(response) {
    console.log("TPV Callback ejecutado: " + JSON.stringify(response));
    if(response && response.status == 'pagado'){
      this.navCtrl.popToRoot();
    }
    else{
      this.navCtrl.pop();
    }
  };

  submitPurchase() {
    this.tpvservice.pay(this.product.id, this.tpvCallback.bind(this));
  };

  back() {
    this.navCtrl.popToRoot();
  };

}
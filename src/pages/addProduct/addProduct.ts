import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';

import { WebService } from '../../providers/webservice';

import { normalizeURL } from 'ionic-angular';

@Component({
    selector: 'page-addProduct',
    templateUrl: 'addProduct.html'
})

export class AddProductPage {

    private urlUploadImages: string;
    private iduser: any;

    formAddProduct: FormGroup;

    addImage1: any;
    addImage2: any;
    addImage3: any;
    addImage4: any;
    addImage5: any;
    addImage6: any;

    isImage1: boolean = false;
    isImage2: boolean = false;
    isImage3: boolean = false;
    isImage4: boolean = false;
    isImage5: boolean = false;
    isImage6: boolean = false;

    public categories: any;
    public pesos: any;

    constructor(
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        public actionSheetCtrl: ActionSheetController,
        public formBuilder: FormBuilder,
        private camera: Camera,
        private transfer: FileTransfer,
        private imageResizer: ImageResizer,
        private webservice: WebService

    ) {
        this.urlUploadImages = 'http://traelo.es/_WebService/upload/products';
        this.iduser = localStorage.getItem('UserId');
        this.formAddProduct = this.formBuilder.group({
            addTitle: ['', Validators.required],
            addDescription: ['', Validators.required],
            addPrice: ['', Validators.required],
            addCategory: ['', Validators.required],
            status: ['', Validators.required],
            kg: ['', Validators.required],
            seguro: [0]
        });

        this.categories = [];
        this.pesos = [];
    }

    ngOnInit() {
        this.loadCategories();
        this.loadPesos();
    }

    loadCategories() {
        this.webservice.getCategories().then(
            (data:any) => {
                this.categories = data;
            },
            (error:any) => {
            }
        );
    };

    loadPesos() {
        this.webservice.getPesos().then(
            (data:any) => {
                this.pesos = data;
            },
            (error:any) => {
            }
        );
    };

    addImage(value) {
        let valueImage: string = value;
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Añadir imagen',
            buttons: [{
                text: 'HACER FOTO',
                icon: 'camera',
                handler: () => {
                    this.camera.getPicture({
                        destinationType: this.camera.DestinationType.FILE_URI,
                        encodingType: this.camera.EncodingType.JPEG,
                        sourceType: this.camera.PictureSourceType.CAMERA,
                        allowEdit: false,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    }).then((cameraUri) => {
                        let optionsResizerCamera = {
                            uri: cameraUri,
                            quality: 90,
                            width: 800,
                            height: 450,
                            fileName: 'imagers.jpg'
                        } as ImageResizerOptions;
                        if(valueImage === 'one'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage1 = true;
                                this.addImage1 = normalizeURL (cameraImage);
                            });
                        }
                        if(valueImage === 'two'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage2 = true;
                                this.addImage2 = normalizeURL (cameraImage);
                            });
                        }
                        if(valueImage === 'three'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage3 = true;
                                this.addImage3 = normalizeURL (cameraImage);
                            });
                        }
                        if(valueImage === 'four'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage4 = true;
                                this.addImage4 = normalizeURL (cameraImage);
                            });
                        }
                        if(valueImage === 'five'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage5 = true;
                                this.addImage5 = normalizeURL (cameraImage);
                            });
                        }
                        if(valueImage === 'six'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage6 = true;
                                this.addImage6 = normalizeURL (cameraImage);
                            });
                        }
                    });
                }
            }, {
                text: 'GALERÍA',
                icon: 'image',
                handler: () => {
                    this.camera.getPicture({
                       
                        destinationType: this.camera.DestinationType.FILE_URI,
                        encodingType: this.camera.EncodingType.JPEG,
                        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                        allowEdit: false,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    }).then((galleryUri) => {
                        let optionsResizerGallery = {
                            uri: galleryUri,
                            quality: 90,
                            width: 800,
                            height: 450,
                            fileName: 'imagers.jpg'
                        } as ImageResizerOptions;
                        if(valueImage === 'one'){
                         
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage1 = true;
                                this.addImage1 = normalizeURL (galleryImage);
                                 
                                
                            });
                        }
                        if(valueImage === 'two'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage2 = true;
                                this.addImage2 = normalizeURL (galleryImage);
                            });
                        }
                        if(valueImage === 'three'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage3 = true;
                                this.addImage3 = normalizeURL (galleryImage);;
                            });
                        }
                        if(valueImage === 'four'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage4 = true;
                                this.addImage4 = normalizeURL (galleryImage);;
                            });
                        }
                        if(valueImage === 'five'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage5 = true;
                                this.addImage5 = normalizeURL (galleryImage);;
                            });
                        }
                        if(valueImage === 'six'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage6 = true;
                                this.addImage6 = normalizeURL (galleryImage);;
                            });
                        }
                    });
                }
            }, {
                text: 'CANCELAR',
                icon: 'close',
                role: 'cancel'
            }]
        });
        actionSheet.present();
    };

    //Subir imágenes 2 a 6
    uploadOptionalImages(productId){
        let pid: string = productId;
        let uploadGallery = this.webservice.getUrlUpload() + 'uptype=gallery&pid=' + pid;

        //Transferir imagen 2
        if(this.isImage2){
            const transferImage2: FileTransferObject = this.transfer.create();
            let optionsTransferImage2: FileUploadOptions = {
                fileKey: 'img2',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage2.upload(this.addImage2, encodeURI(uploadGallery), optionsTransferImage2)
                .then((data) => {
                console.log("Upload img 2 ok: " + JSON.stringify(data));
            }, (error) => {
                alert("Error al subir imagen 2: " + JSON.stringify(error));
                console.log("upload 2 error source " + error.source);
                console.log("upload 2 error target " + error.target);
            });
        }
        //Transferir imagen 3
        if(this.isImage3){
            const transferImage3: FileTransferObject = this.transfer.create();
            let optionsTransferImage3: FileUploadOptions = {
                fileKey: 'img3',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage3.upload(this.addImage3, encodeURI(uploadGallery), optionsTransferImage3)
                .then((data) => {
                console.log("Upload img 3 ok: " + JSON.stringify(data));
            }, (error) => {
                alert("Error al subir imagen 3: " + JSON.stringify(error));
            });
        }
        //Transferir imagen 4
        if(this.isImage4){
            const transferImage4: FileTransferObject = this.transfer.create();
            let optionsTransferImage4: FileUploadOptions = {
                fileKey: 'img4',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage4.upload(this.addImage4, encodeURI(uploadGallery), optionsTransferImage4)
                .then((data) => {
                console.log("Upload img 4 ok: " + JSON.stringify(data));
            }, (error) => {
                alert("Error al subir imagen 4: " + JSON.stringify(error));
            });
        }
        
        //Transferir imagen 5
        if(this.isImage5){
            const transferImage5: FileTransferObject = this.transfer.create();
            let optionsTransferImage5: FileUploadOptions = {
                fileKey: 'img5',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage5.upload(this.addImage5, encodeURI(uploadGallery), optionsTransferImage5)
                .then((data) => {
                console.log("Upload img 5 ok: " + JSON.stringify(data));
            }, (error) => {
                alert("Error al subir imagen 5: " + JSON.stringify(error));
            });
        }
        
        //Transferir imagen 6
        if(this.isImage6){
            const transferImage6: FileTransferObject = this.transfer.create();
            let optionsTransferImage6: FileUploadOptions = {
                fileKey: 'img6',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage6.upload(this.addImage6, encodeURI(uploadGallery), optionsTransferImage6)
                .then((data) => {
                console.log("Upload img 6 ok: " + JSON.stringify(data));
            }, (error) => {
                alert("Error al subir imagen 6: " + JSON.stringify(error));
            });
        }
    };

    submitAddProduct() {
        console.log("WOOOO submit product iduser: " + this.iduser);
        if(!this.isImage1){
            let alert = this.alertCtrl.create({
                title: 'Antes de publicar...',
                subTitle: 'Es necesario añadir al menos una imagen del producto.',
                buttons: [{
                    text: 'ENTENDIDO',
                    role: 'cancel'
                }]
            });
            alert.present();
        }else{
            let value = this.formAddProduct.value;
            let loading = this.loadingCtrl.create({
                content: 'Publicando producto, espere...'
            });
            loading.present();

            setTimeout(() => {

                this.webservice.addProduct(this.iduser, 
                    value.addTitle ? value.addTitle.trim():null, 
                    value.addDescription ? value.addDescription.trim():null, 
                    value.kg, 
                    value.addPrice, 
                    this.isImage1, this.isImage2, this.isImage3, this.isImage4, this.isImage5, this.isImage6,
                    value.addCategory,
                    value.status,
                    value.seguro
                ).then((dataInsert: any) => {
                    console.log("WOOOO insert ok id: " + dataInsert.id);

                    //let urlUpload = 'http://traelo.es/_WebService/upload/upload.php?access_token=ZihutXBr0SkGBzt63T5wDDJOg3Y5DCgt&';
                    let uploadAdd = this.webservice.getUrlUpload() + 'uptype=add&pid=' + dataInsert.id;
                    
                    //Transferir imagen 1
                    const transferImage1: FileTransferObject = this.transfer.create();
                    let optionsTransferImage1: FileUploadOptions = {
                        fileKey: 'img1',
                        chunkedMode: false,
                        mimeType: 'image/jpeg'
                    }
                    transferImage1.upload(this.addImage1, encodeURI(uploadAdd), optionsTransferImage1)
                    .then((data) => {
                        console.log("Upload img 1 ok: " + JSON.stringify(data));
                        
                        //No subimos el resto de imágenes hasta que no se guarde la primera que es la que crea el directorio
                        this.uploadOptionalImages(dataInsert.id);

                        if(dataInsert.id){
                            loading.dismiss();
                            let alert = this.alertCtrl.create({
                                title: 'El producto se ha añadido',
                                subTitle: 'Su producto se ha publicado correctamente. Le informamos que será revisado para ver si cumple con las condiciones de uso, de lo contrario será eliminado permanentemente.',
                                buttons: [{
                                    text: 'IR A LISTA DE PRODUCTOS',
                                    handler: () => {
                                        this.navCtrl.popToRoot();
                                    }
                                }]
                            });
                            alert.present();
                        }

                    //Control de error de imagen 1
                    }, (error) => {
                        console.log("upload imagen1 error " + JSON.stringify(error));
                        loading.dismiss();

                        //Mostrar error de subida de imagen
                        let alert = this.alertCtrl.create({
                            title: 'Vaya...',
                            subTitle: 'No se ha podido subir la imagen principal del producto.',
                            buttons: [{
                                text: 'CERRAR',
                                role: 'cancel'
                            }]
                        });
                        alert.present();
                    }); //Fin transfer imagen 1
                    
                });//Fin guardar producto
            }, 2000);
        }
    };

}
/**
 * Se utiliza para pagos con TPV Virtual desde APP cordova utilizando el plugin Inappbrowser
 * Abre la url de terceros en una ventana aparte de nuestra app y captura el resultado.
 *
 */
import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { UserService } from '../providers/userservice';
 
@Injectable()

export class TpvService {

    userLogged: any;
    tpvUrl: string;
    tpvParams: any;
    //Esto son parte de las url configuradas como retorno de tpv para saber si el pago fue bien o erróneo
    SUCCESS_URL: string = 'payment_ok';
    CANCEL_URL: string = 'payment_ko';
    
    loginCallback: any;

    //Para detectar si el pago fue hecho y no mostrar error al salir
    pagoRealizado: any;
    pagoCancelado: any;

    loginWindow: any;
    private startTime: any;

    constructor(
        private inAppBrowser: InAppBrowser,
        private userService: UserService
    ) {
        this.userLogged = this.userService.getUser();
    }

    /**
     * Inicializa las variables de configuración
     */
    init(data) {
        this.pagoRealizado = false;
        this.pagoCancelado = false;

        if (data.url) {
            this.tpvUrl = data.url;
        } else {
            throw 'Parámetro Url del tpv no definido en init()';
        }

        if (data.params) {
            this.tpvParams = data.params;
        } else {
            throw 'Parámetros del tpv no definido en init()';
        }
    }


    /**
     * Pago en tpv: muestra la pagina de tpv para que el usuario se logue y pague
     *
     * @param callback - Callback para devolver el resultado de esta operación
     * @returns {*}
     */
    pay(idProducto, callback) {
        let url = "http://traelo.es/public/api/pagotpv/"+idProducto+"?u="+this.userLogged.id;
        this.init({url: url, params: {} });

        if (!this.tpvUrl) {
            return callback({status: 'unknown', error: 'Url de tpv no definida'});
        }

        this.loginCallback = callback;
        this.startTime = new Date().getTime();
        //Abrimos la popup con la url del tpv:
        this.loginWindow = this.openWindow(this.tpvUrl, this.tpvParams);
    }

    // Inappbrowser load start handler: Used when running in Cordova only
    loginWindow_loadStartHandler(event) {
        var url = event.url;
        //console.log("*********loginWindow_loadStartHandler URL devuelta: " + url);
        if (url.indexOf(this.SUCCESS_URL) > 0 || url.indexOf(this.CANCEL_URL) > 0) {
            // When we get the access token fast, the login window (inappbrowser) is still opening with animation
            // in the Cordova app, and trying to close it while it's animating generates an exception. Wait a little...
            var timeout = 600 - (new Date().getTime() - this.startTime);
            setTimeout(() => {
                this.loginWindow.close();
            }, timeout > 0 ? timeout : 0);

            this.finalCallback(url, null);
        }
    }

    // Inappbrowser exit handler: Used when running in Cordova only
    loginWindow_exitHandler() {
        this.loginWindow = null;
        //Si estamos saliendo y no se ha hecho el pago ni se ha cancelado, devolvemos error:
        if(!this.pagoRealizado && !this.pagoCancelado)
            this.finalCallback(null, {status: 'cancelado', error: 'El usuario ha cancelado el pago'});
    }

    //Funcion para abrir la url en otra ventana y además hacer un POST, ya que el TPV necesita
    //llamar a la url con el método post:
    openWindow(url, params) {
        //Abrimos ventana vacía con el formulario de envío:
        var browser = this.inAppBrowser.create(url, '_blank', {location: 'no'});

        //Le añadimos los eventos que controlan cuando se devuelve la respuesta:
        browser.on('loadstart').subscribe((event) => {
            this.loginWindow_loadStartHandler(event);
        });
        
        browser.on('exit').subscribe((event) => {
            this.loginWindow_exitHandler();
        });

        return browser;       
    }

    /**
     * Esta función se ejecuta cuando el pago se ha realizado o cancelado y 
     * se utiliza para devolver el resultado de la funcion login
     *
     * @param url - La url llamada por tpv para devolver el resultado
     * 
     */
    finalCallback(url, response) {
        //Devolver la respuesta desde tpv:
        var queryString,
            obj;

        //Si viene directamente la respuesta la devolvemos y salimos:
        if(response && this.loginCallback)
        {
            //console.log("Respuesta directa, salimos");
            return this.loginCallback(response);
        }

        if (url.indexOf(this.SUCCESS_URL) > 0) {
            this.pagoRealizado = true;
            //console.log("*********finalCallback URL devuelta: " + url);
            queryString = url.substring(url.indexOf('?') + 1);
            obj = this.parseQueryString(queryString);
            if (this.loginCallback) 
                this.loginCallback({status: 'pagado'});
        
        } else if (url.indexOf(this.CANCEL_URL) > 0) {
            this.pagoCancelado = true;
            queryString = url.substring(url.indexOf('?') + 1);
            if (this.loginCallback) 
                this.loginCallback({status: 'cancelado', error: 'El usuario ha cancelado el pago'});
        
        } else {
            if (this.loginCallback) 
                this.loginCallback({status: 'not_authorized', error: 'Error desconocido'});
        }
    }

    //Se utiliza para extraer los parametros de una url en un objeto clave-valor
    parseQueryString(queryString) {
        var qs = decodeURIComponent(queryString),
            obj = {},
            params = qs.split('&');

        params.forEach(function (param) {
            var splitter = param.split('=');
            obj[splitter[0]] = splitter[1];
        });
        return obj;
    }

}
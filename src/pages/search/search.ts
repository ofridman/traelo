import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WebService } from '../../providers/webservice';
import { ResultsPage } from '../../pages/results/results';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})

export class SearchPage {

  formSearch: FormGroup;
  //private user: any;
  public regions: any;
  public provinces: any;
  public categories: any;
  public userTypes: any;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private webservice: WebService,
  ) {
    
    this.formSearch = this.formBuilder.group({
      searchText: [''],
      category: [''],
      province: [''],
      region: [''],
      status: [''],
      seller: [''],
    });

    this.categories = [];
    this.regions = [];
    this.provinces = [];

    this.userTypes = [
        {id:1, name: 'Particular'},
        {id:2, name: 'Profesional'}
    ];
  }

  ngOnInit() {
        //Cargar comunidades y provincias:
        this.loadRegions();
        this.loadProvinces();
        this.loadCategories();
  };

  loadCategories() {
    this.webservice.getCategories().then(
      (data:any) => {
        this.categories = data;
      },
      (error:any) => {
      }
    );
  };

  loadRegions() {
    this.webservice.getRegions().then(
      (data:any) => {
        this.regions = data;
      },
      (error:any) => {
      }
    );
  };

  loadProvinces() {
    this.webservice.getProvinces().then(
      (data:any) => {
        this.provinces = data;
      },
      (error:any) => {
      }
    );
  };

  //Filtrar provincias por comunidad seleccionada:
  filterProvinces() {
    if(this.formSearch.value.region){
      return this.provinces.filter((item) => {
        return item.comunidad_id == this.formSearch.value.region;
      });
    }
    else
      return this.provinces;
  };


  submitSearch() {

    let value = this.formSearch.value;
    if(!value.searchText && !value.category && !value.region && !value.province){
      const alert = this.alertCtrl.create({
        title: 'Atención...',
        message: 'No ha introducido ningún criterio de búsqueda',
        buttons: [{
          text: 'ENTENDIDO',
          role: 'cancel'
        }]
      });
      alert.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      content: 'Buscando, espere...'
    });
    loading.present();

    
    let searchData = {
      text: value.searchText,
      category: value.category,
      region: value.region,
      province: value.province,
      status: value.status,
      seller: value.seller
    };

    setTimeout(() => {
      this.webservice.productSearch(searchData).then(
          (data:any) => {
            loading.dismissAll();
            console.log("Resultados buscar: " + JSON.stringify(data));
            this.navCtrl.push(ResultsPage, {
              productsList: data
            });
          },
          err => {
            loading.dismissAll();
            console.log("Error al buscar: " + JSON.stringify(err));
            const alert = this.alertCtrl.create({
              title: 'Vaya...',
              message: 'No se ha podido completar la búsqueda...',
              buttons: [{
                text: 'VOLVER A INTENTARLO',
                role: 'cancel'
              }]
            });
            alert.present();
          }
      );
    }, 1000); 
    
  };

  back() {
    this.navCtrl.popToRoot();
  };

}
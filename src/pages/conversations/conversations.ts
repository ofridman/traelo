import { Component } from '@angular/core';
import { Platform, NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';

import { ChatPage } from '../../pages/chat/chat';

import { WebService } from '../../providers/webservice';
import { ChatService } from '../../providers/chatservice';

@Component({
  selector: 'page-conversations',
  templateUrl: 'conversations.html'
})

export class ConversationsPage {

    public conversations: any;
    private userid: any;
    private urlUploadImages: any;

    private isBackButtonClicked: boolean = false;

    isEmpty: boolean = false;
    classSelected: string = "selected";
    itemSelectId: any = null;
    itemSelect: number = null;
    isDeleting: boolean = false;

    constructor(
        platform: Platform,
        public navCtrl: NavController,
        private navParams: NavParams,
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        private webservice: WebService,
        private chatservice: ChatService
    ) {
        /*platform.registerBackButtonAction(() => {
            if(this.isDeleting) {
                this.itemSelectId = null;
                this.itemSelect = null;
                this.isDeleting = false;
            }
        });*/
        /*
        let loadingConversations = this.loadingCtrl.create({
            content: "Cargando..."
        });
        loadingConversations.present();
        */
        this.urlUploadImages = webservice.getUrlProductImages();
        this.userid = this.navParams.get('userid');
        this.conversations = [];
    }

    ngOnInit() {
        if(this.userid)
            this.loadConversations(this.userid);
        else
            console.log("Conversaciones: no hay usuario");
    };

    loadConversations(userid) {
        //console.log("Cargando conversaciones para usuario: " + userid);
        const loading = this.loadingCtrl.create({
            content: 'Cargando conversaciones...'
        });
        loading.present();

        setTimeout(() => {
          this.webservice.getUserConversations(userid)
          .then(
            data => {
              loading.dismissAll();
              this.conversations = data;
              //console.log("conversaciones: " + JSON.stringify(this.conversations));
            },
            err => {
              loading.dismissAll();
              console.log("Error al cargar conversaciones: " + JSON.stringify(err));
              const alert = this.alertCtrl.create({
                message: 'Vaya! Lo sentimos, pero no se han podido cargar las conversaciones. Inténtelo de nuevo más tarde...',
                buttons: [{
                  text: 'OK',
                  role: 'cancel'
                }]
              });
              alert.present();
            }
          );

        }, 1200);
    };

    orderConversations() {
        return (this.conversations || []).sort((a: any, b: any) => {
            let aLatest = a.latest_message ? a.latest_message.id : 0;
            let bLatest = b.latest_message ? b.latest_message.id : 0;

            return aLatest > bLatest  ? -1 : 1;
        });
    }

    getImgItem(conversation) {
        if(!conversation.product || !conversation.product.img1) 
            return 'assets/img/nophoto.png';
        return this.urlUploadImages+'/'+conversation.product.id+'/'+conversation.product.img1;
    }

    getUserName(conversation) {
        if(conversation.creator && conversation.creator.id != this.userid)
            return conversation.creator.name;
        else if(conversation.seller && conversation.seller.id != this.userid)
            return conversation.seller.name;
    }

    getLastMsg(conversation) {
        return conversation.latest_message ? conversation.latest_message.message : '';
    }

    getLastDate(conversation) {
        return conversation.latest_message ? conversation.latest_message.created_at : conversation.updated_at;
    }

    startChat(conversation) {
        if(this.isDeleting) {
            console.log("startChat está eliminando, salimos");
            return;
        }
        this.navCtrl.push(ChatPage, {
            conversation: conversation,
            userid: this.userid
        });
    };

    itemSelected(conversationId, itemIndex) {
        this.itemSelectId = conversationId;
        this.itemSelect = itemIndex;
        this.isDeleting = true;
    };

    deleteConversation() {
        this.webservice.removeUserConversation(this.itemSelectId, 1).then((data: any) => {
            if(data.success) {
                this.conversations.splice(this.itemSelect, 1);
                this.itemSelectId = null;
                this.itemSelect = null;
                this.isDeleting = false;
                if(data.conversationslength === 0) {
                    this.isEmpty = true;
                }
            }
        });
    };

    unreadMessages(conversation) {
        return this.chatservice.getMessages(conversation.id).length;
    }

}
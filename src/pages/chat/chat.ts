import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { Content } from 'ionic-angular';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';

import { WebService } from '../../providers/webservice';
import { ChatService } from '../../providers/chatservice';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html'
})

export class ChatPage {

    @ViewChild('content') content:any;

    public conversation: any;
    public product: any;
    public messages: any;
    public myUser: any;
    public otherUser: any;
    private urlUploadAvatars: string;
    private userid: any;
    message: string;
    isLoading: boolean = true;
    subscription: Subscription;

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        public alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
        private webservice: WebService,
        private chatservice: ChatService,
        private ref: ChangeDetectorRef
    ) {
        this.messages = [];
        this.urlUploadAvatars = webservice.getUrlAvatarImages();
        this.conversation = this.navParams.get('conversation');
        this.userid = this.navParams.get('userid'); //user logged
        this.product = this.conversation ? this.conversation.product : this.navParams.get('product');
        
        //subscribir a los cambios de nuevos mensajes en tiempo real
        this.subscription = this.chatservice.onChangeMessages().subscribe(unreadMessages => { 
          console.log("Chat - messages cambia: " + unreadMessages.length);
          if(unreadMessages.length)
            this.pushMessages(unreadMessages);
        });

    };

    ngOnDestroy() {
        //unsubscribe
        this.subscription.unsubscribe();
    };

    ngOnInit() {
        //Si hay conversation, venimos de la lista de conversaciones:
        if(this.conversation && this.conversation.id){
            this.loadData(this.conversation, true);
        }
        //Si viene de la página de detalles de un producto buscamos la conversation:
        else if(this.product){
            console.log("Buscando conversación por producto...");
            //Buscar conversation por usuario logado y producto:
            this.webservice.findConversation(this.product.id, this.userid)
            .then((data) => {
                console.log("conversation find data: " + JSON.stringify(data));
                if(data && data.length>0){
                    this.conversation = data[0];
                    this.loadData(this.conversation, true);
                }
            });
        }

        //Refrescar cada 5 segundos: Esto provoca muchos errores. Hay que refrescar los chats por notificaciones push.
        // if(this.conversation.id){
        //     setInterval(() => {
        //         this.getAllMessages();
        //     }, 5000);
        // }
    };

    //Cargar datos cuando tengamos objeto de conversation:
    loadData(conversation, refreshMessages) {
        this.myUser = this.getMyUser(conversation);
        this.otherUser = this.getOtherUser(conversation);
        this.isLoading = false;
        if(!refreshMessages) return;
        this.getAllMessages();
    };

    //Cargar mensajes:
    getAllMessages() {
        let count = this.messages.length;
        this.webservice.getConversationMessages(this.conversation.id, this.userid)
        .then((data) => {
            console.log("Refrescando mensajes... Count: " + data.length);
            this.messages = data;
            this.ref.detectChanges();//Para forzar a refrescar la vista
            //Borrar mensajes no leidos:
            this.chatservice.clearMessages(this.conversation.id);
            //Solo hacer scroll cuando haya nuevos
            if(count!=this.messages.length){ 
                this.doScroll(100);
            }
        });
    };

    //añadir nuevos mensajes en tiempo real a la ventana del chat:
    pushMessages(unreadMessages) {
        let count = this.messages.length;
        let news = false;
        for( var i=0; i<unreadMessages.length; i++){
          if(unreadMessages[i] && (unreadMessages[i].conversation_id == this.conversation.id)){
            let copy = JSON.parse(JSON.stringify(unreadMessages[i]));
            this.messages.push(copy);
            news = true;
          }
        }
        if(news){
            this.ref.detectChanges();//Para forzar a refrescar la vista
            //Solo hacer scroll cuando haya nuevos
            this.doScroll(100);
            //Borrar mensajes no leidos:
            this.chatservice.clearMessages(this.conversation.id);
        }
    }

    getOtherUser(conversation) {
        if(conversation.creator && conversation.creator.id != this.userid)
            return conversation.creator;
        else if(conversation.seller && conversation.seller.id != this.userid)
            return conversation.seller;
        else return {}; //Nunca debería llegar aquí. Habría inconsistencia de datos
    }

    getMyUser(conversation) {
        if(conversation.creator && conversation.creator.id == this.userid)
            return conversation.creator;
        else if(conversation.seller && conversation.seller.id == this.userid)
            return conversation.seller;
        else return {}; //Nunca debería llegar aquí. Habría inconsistencia de datos
    }

    getUserAvatar(user) {
        if(user.avatar)
            return this.urlUploadAvatars + '/' + user.id + "/" + user.avatar;
        else
            return 'assets/icon/no-avatar.png';
    }

    isMyMessage(message) {
        return message.user_id == this.userid;
    }

    showError(msgError) {
        const alert = this.alertCtrl.create({
          message: msgError,
          buttons: [{
            text: 'OK',
            role: 'cancel'
          }]
        });
        alert.present();
    };

    //Enviar mensaje
    send(msg) {
        this.message = '';

        //Primero nos aseguramos que hay conversation:
        let promise1 = new Promise((resolve, reject) => {
            if(this.conversation)
                return resolve(this.conversation);

            const loading = this.loadingCtrl.create({
              content: 'Conectando...'
            });
            loading.present();

            //Si no hay conversación, primero hay que crearla:
            let newConver = {
                creator_id: this.userid,
                product_id: this.product.id,
                populate: true //esto es para que nos devuelva el nuevo objeto con sus relaciones populadas
            };
            this.webservice.createConversation(newConver).then(
                (data: any) => {
                    this.conversation = data;
                    this.loadData(this.conversation, false);//el segundo parámetro descarta el refresco de mensajes
                    loading.dismissAll();
                    resolve(this.conversation);
                },
                (err: any) => {
                    loading.dismissAll();
                    this.showError('Parece que hay problemas para iniciar la conversación...');
                    reject(err);
                }
            );
        });

        //Cuando sepamos que hay conversation enviamos el mensaje:
        promise1.then((data: any) => {
            //Creamos el objeto mensaje:
            let newMsg = {
                conversation_id: this.conversation.id,
                user_id: this.userid,
                message: msg,
                status_id: 1
            };
            //Enviamos el mensaje al servidor
            this.webservice.sendMessage(newMsg)
            .then(
                (data: any) => {
                    this.getAllMessages();//refresca la lista para añadir el nuevo
                },
                (err: any) => {
                    this.showError('Parece que tu mensaje no ha podido enviarse...');
                }
            );
        });//Fin resultado promise1
    };

    doScroll (scrollingTime){
        setTimeout(() => {
            if(this.content)
                this.content.scrollToBottom(scrollingTime);
        }, 300); //Si no esperamos este tiempo, a veces no hace scroll. 
    };

    ionViewDidEnter(){
        this.doScroll(300);
    }
}
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ImageResizer, ImageResizerOptions } from '@ionic-native/image-resizer';

import { WebService } from '../../providers/webservice';

@Component({
    selector: 'page-editproduct',
    templateUrl: 'editproduct.html'
})

export class EditProductPage {

    private urlUploadImages: string;
    private idproduct: any;
    public product: any;
    public categories: any;
    public pesos: any;

    formProduct: FormGroup;

    addImage1: any;
    addImage2: any;
    addImage3: any;
    addImage4: any;
    addImage5: any;
    addImage6: any;

    isImage1: boolean = false;
    isImage2: boolean = false;
    isImage3: boolean = false;
    isImage4: boolean = false;
    isImage5: boolean = false;
    isImage6: boolean = false;

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        public actionSheetCtrl: ActionSheetController,
        public formBuilder: FormBuilder,
        private camera: Camera,
        private transfer: FileTransfer,
        private imageResizer: ImageResizer,
        private webservice: WebService
    ) {
        this.urlUploadImages = webservice.getUrlProductImages();
        this.idproduct = this.navParams.get('idproduct');
        this.product = {};

        this.formProduct = this.formBuilder.group({
            title: ['', Validators.required],
            description: ['', Validators.required],
            price: [0, Validators.required],
            category: [1, Validators.required],
            status: [1, Validators.required],
            kg: [0, Validators.required],
            seguro: [0]
        });

        this.categories = [];
        this.pesos = [];
    }

    ngOnInit() {
        this.loadProductData();
        this.loadCategories();
        this.loadPesos();
    };

    loadCategories() {
        this.webservice.getCategories().then(
            (data:any) => {
                this.categories = data;
            },
            (error:any) => {
            }
        );
    };

    loadPesos() {
        this.webservice.getPesos().then(
            (data:any) => {
                this.pesos = data;
                console.log("pesos: " + JSON.stringify(data));
            },
            (error:any) => {
            }
        );
    };

    loadProductData() {
        setTimeout(() => {
            this.webservice.getProductDetails(this.idproduct)
            .then((data: any) => {

                if(!data || !data.id){
                  let alert = this.alertCtrl.create({
                    subTitle: 'Parece que este producto ya no se encuentra...',
                    buttons: [{
                      text: 'CERRAR',
                      role: 'cancel'
                    }],
                    enableBackdropDismiss: false,
                  });
                  alert.present();
                  return;
                }
                //Producto cargado, asignar valores...
                this.product = data;
                this.formProduct.get('title').setValue(this.product.title);
                this.formProduct.get('description').setValue(this.product.description);
                this.formProduct.get('price').setValue(this.product.price);
                this.formProduct.get('category').setValue(this.product.category);
                this.formProduct.get('status').setValue(this.product.status);
                this.formProduct.get('kg').setValue(this.product.kg);
                this.formProduct.get('seguro').setValue(this.product.seguro==true?1:0);
                if(data.img1){
                    this.addImage1 = this.urlUploadImages + '/' + data.id + '/' + data.img1;
                }
                if(data.img2){
                    this.addImage2 = this.urlUploadImages + '/' + data.id + '/' + data.img2;
                }
                if(data.img3){
                    this.addImage3 = this.urlUploadImages + '/' + data.id + '/' + data.img3;
                }
                if(data.img4){
                    this.addImage4 = this.urlUploadImages + '/' + data.id + '/' + data.img4;
                }
                if(data.img5){
                    this.addImage5 = this.urlUploadImages + '/' + data.id + '/' + data.img5;
                }
                if(data.img6){
                    this.addImage6 = this.urlUploadImages + '/' + data.id + '/' + data.img6;
                }
            });
        }, 100);
    };

    addImage(value) {
        let valueImage: string = value;
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Añadir imagen',
            buttons: [{
                text: 'HACER FOTO',
                icon: 'camera',
                handler: () => {
                    this.camera.getPicture({
                        destinationType: this.camera.DestinationType.FILE_URI,
                        encodingType: this.camera.EncodingType.JPEG,
                        sourceType: this.camera.PictureSourceType.CAMERA,
                        allowEdit: false,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    }).then((cameraUri) => {

                        let optionsResizerCamera = {
                            uri: cameraUri,
                            quality: 90,
                            width: 800,
                            height: 450,
                            fileName: 'imagers.jpg'
                        } as ImageResizerOptions;

                        if(valueImage === 'one'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage1 = true;
                                this.addImage1 = cameraImage;
                            });
                        }
                        if(valueImage === 'two'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage2 = true;
                                this.addImage2 = cameraImage;
                            });
                        }
                        if(valueImage === 'three'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage3 = true;
                                this.addImage3 = cameraImage;
                            });
                        }
                        if(valueImage === 'four'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage4 = true;
                                this.addImage4 = cameraImage;
                            });
                        }
                        if(valueImage === 'five'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage5 = true;
                                this.addImage5 = cameraImage;
                            });
                        }
                        if(valueImage === 'six'){
                            this.imageResizer.resize(optionsResizerCamera).then((cameraImage: string) => {
                                this.isImage6 = true;
                                this.addImage6 = cameraImage;
                            });
                        }
                    });
                }
            }, {
                text: 'GALERÍA',
                icon: 'image',
                handler: () => {
                    this.camera.getPicture({
                        destinationType: this.camera.DestinationType.FILE_URI,
                        encodingType: this.camera.EncodingType.JPEG,
                        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                        allowEdit: false,
                        saveToPhotoAlbum: false,
                        correctOrientation: true
                    }).then((galleryUri) => {

                        let optionsResizerGallery = {
                            uri: galleryUri,
                            quality: 90,
                            width: 800,
                            height: 450
                        } as ImageResizerOptions;

                        if(valueImage === 'one'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage1 = true;
                                this.addImage1 = galleryImage;
                            });
                        }
                        if(valueImage === 'two'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage2 = true;
                                this.addImage2 = galleryImage;
                            });
                        }
                        if(valueImage === 'three'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage3 = true;
                                this.addImage3 = galleryImage;
                            });
                        }
                        if(valueImage === 'four'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage4 = true;
                                this.addImage4 = galleryImage;
                            });
                        }
                        if(valueImage === 'five'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage5 = true;
                                this.addImage5 = galleryImage;
                            });
                        }
                        if(valueImage === 'six'){
                            this.imageResizer.resize(optionsResizerGallery).then((galleryImage: string) => {
                                this.isImage6 = true;
                                this.addImage6 = galleryImage;
                            });
                        }
                    });
                }
            }, {
                text: 'CANCELAR',
                icon: 'close',
                role: 'cancel'
            }]
        });
        actionSheet.present();
    };

    //Subir imágenes: devuelve una promesa
    uploadImages(product) {
        let pid: string = product.id;
        let uploadEdit = this.webservice.getUrlUpload() + 'uptype=edit&pid=' + pid;
        //let uploadGallery = this.webservice.getUrlUpload() + 'uptype=gallery&pid=' + pid;

        //Transferir imagen 1 o principal
        let promise1 = new Promise((resolve, reject) => {
            if(!this.isImage1)
                return resolve(0);

            let url1 = uploadEdit + '&filename=' + product.img1;
            const transferImage1: FileTransferObject = this.transfer.create();
            let optionsTransferImage1: FileUploadOptions = {
                fileKey: 'img1',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage1.upload(this.addImage1, encodeURI(url1), optionsTransferImage1)
            .then((data) => {
                console.log("Upload img 1 ok: " + JSON.stringify(data));
                return resolve(1);
            }, (error) => {
                console.log("Error al subir imagen 1: " + JSON.stringify(error));
                return reject(0);
            });
        });

        //Transferir imagen 2
        let promise2 = new Promise((resolve, reject) => {
            if(!this.isImage2)
                return resolve(0);

            let url2 = uploadEdit + '&filename=' + product.img2;
            const transferImage2: FileTransferObject = this.transfer.create();
            let optionsTransferImage2: FileUploadOptions = {
                fileKey: 'img2',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage2.upload(this.addImage2, encodeURI(url2), optionsTransferImage2)
            .then((data) => {
                console.log("Upload img 2 ok: " + JSON.stringify(data));
                return resolve(1);
            }, (error) => {
                console.log("Error al subir imagen 2: " + JSON.stringify(error));
                return reject(0);
            });
        });

        //Transferir imagen 3
        let promise3 = new Promise((resolve, reject) => {
            if(!this.isImage3)
                return resolve(0);

            let url3 = uploadEdit + '&filename=' + product.img3;
            const transferImage3: FileTransferObject = this.transfer.create();
            let optionsTransferImage3: FileUploadOptions = {
                fileKey: 'img3',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage3.upload(this.addImage3, encodeURI(url3), optionsTransferImage3)
            .then((data) => {
                console.log("Upload img 3 ok: " + JSON.stringify(data));
                return resolve(1);
            }, (error) => {
                console.log("Error al subir imagen 3: " + JSON.stringify(error));
                return reject(0);
            });
        });

        //Transferir imagen 4
        let promise4 = new Promise((resolve, reject) => {
            if(!this.isImage4)
                return resolve(0);

            let url4 = uploadEdit + '&filename=' + product.img4;
            const transferImage4: FileTransferObject = this.transfer.create();
            let optionsTransferImage4: FileUploadOptions = {
                fileKey: 'img4',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage4.upload(this.addImage4, encodeURI(url4), optionsTransferImage4)
            .then((data) => {
                console.log("Upload img 4 ok: " + JSON.stringify(data));
                return resolve(1);
            }, (error) => {
                console.log("Error al subir imagen 4: " + JSON.stringify(error));
                return reject(0);
            });
        });
        
        //Transferir imagen 5
        let promise5 = new Promise((resolve, reject) => {
            if(!this.isImage5)
                return resolve(0);

            let url5 = uploadEdit + '&filename=' + product.img5;
            const transferImage5: FileTransferObject = this.transfer.create();
            let optionsTransferImage5: FileUploadOptions = {
                fileKey: 'img5',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage5.upload(this.addImage5, encodeURI(url5), optionsTransferImage5)
            .then((data) => {
                console.log("Upload img 5 ok: " + JSON.stringify(data));
                return resolve(1);
            }, (error) => {
                console.log("Error al subir imagen 5: " + JSON.stringify(error));
                return reject(0);
            });
        });
        
        //Transferir imagen 6
        let promise6 = new Promise((resolve, reject) => {
            if(!this.isImage6)
                return resolve(0);

            let url6 = uploadEdit + '&filename=' + product.img6;
            const transferImage6: FileTransferObject = this.transfer.create();
            let optionsTransferImage6: FileUploadOptions = {
                fileKey: 'img6',
                chunkedMode: false,
                mimeType: 'image/jpeg'
            }
            transferImage6.upload(this.addImage6, encodeURI(url6), optionsTransferImage6)
            .then((data) => {
                console.log("Upload img 6 ok: " + JSON.stringify(data));
                return resolve(1);
            }, (error) => {
                console.log("Error al subir imagen 6: " + JSON.stringify(error));
                return reject(0);
            });
        });

        return Promise.all([promise1, promise2, promise3, promise4, promise5, promise6]);
    };

    submitProduct() {
        console.log("WOOOO submit product id: " + this.idproduct);
        if(!this.addImage1){
            let alert = this.alertCtrl.create({
                title: 'Antes de publicar...',
                subTitle: 'Es necesario añadir al menos una imagen del producto.',
                buttons: [{
                    text: 'ENTENDIDO',
                    role: 'cancel'
                }]
            });
            alert.present();
            return;
        }

        let value = this.formProduct.value;
        //Asignar valores que han podido cambiar
        this.product.title = value.title;
        this.product.description = value.description;
        this.product.price = value.price;
        if(this.isImage1){
            this.product.img1 = 'img1-' + Date.now() +'.jpg';
        }
        if(this.isImage2){
            this.product.img2 = 'img2-' + Date.now() +'.jpg';
        }
        if(this.isImage3){
            this.product.img3 = 'img3-' + Date.now() +'.jpg';
        }
        if(this.isImage4){
            this.product.img4 = 'img4-' + Date.now() +'.jpg';
        }
        if(this.isImage5){
            this.product.img5 = 'img5-' + Date.now() +'.jpg';
        }
        if(this.isImage6){
            this.product.img6 = 'img6-' + Date.now() +'.jpg';
        }
        this.product.category = value.category;
        this.product.kg = value.kg;
        this.product.status = value.status;
        this.product.seguro = value.seguro;
        console.log("kg: " + value.kg);

        let loading = this.loadingCtrl.create({
            content: 'Actualizando producto, espere...'
        });
        loading.present();

        setTimeout(() => {

            this.webservice.updateProduct(this.product).then((dataUpdate: any) => {
                console.log("WOOOO update ok id: " + dataUpdate.id);
                if(!dataUpdate.id){
                    loading.dismiss();
                    let alert = this.alertCtrl.create({
                        title: 'Oh oh...',
                        subTitle: 'Parece que no se ha podido actualizar el producto. Inténtelo de nuevo más tarde.',
                        buttons: [{
                            text: 'ENTENDIDO',
                            role: 'cancel'
                        }]
                    });
                    alert.present();
                    return;
                }

                //Subir imágenes:
                this.uploadImages(this.product)
                .then(values => {
                    console.log("Resultado actualizar imágenes: " + JSON.stringify(values));

                    loading.dismiss();
                    let alert = this.alertCtrl.create({
                        title: 'El producto se ha actualizado',
                        subTitle: 'Le informamos que será revisado para ver si cumple con las condiciones de uso',
                        buttons: [{
                            text: 'CERRAR',
                            handler: () => {
                                this.navCtrl.pop();
                            }
                        }]
                    });
                    alert.present();

                }, (error) => {
                    console.log("Error al subir imagenes: " + JSON.stringify(error));
                    loading.dismiss();
                    let alert = this.alertCtrl.create({
                        title: 'Oh oh...',
                        subTitle: 'Parece que ha fallado la transferencia de alguna de las imágenes.',
                        buttons: [{
                            text: 'CERRAR',
                            role: 'cancel'
                        }]
                    });
                    alert.present();
                    return;
                });

                
            });//Fin guardar producto
        }, 2000);
        
    };

}
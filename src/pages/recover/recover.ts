import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { LoginPage } from '../../pages/login/login';
import { WebService } from '../../providers/webservice';

@Component({
  selector: 'page-recover',
  templateUrl: 'recover.html'
})

export class RecoverPage {

  formRecover: FormGroup;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    public webservice: WebService
  ) {

  	this.formRecover = this.formBuilder.group({
      email: ['', Validators.required]
    });

  }


  submitEmail() {
    let value = this.formRecover.value;

    if(!value.email){
      const alert = this.alertCtrl.create({
        message: 'Por favor, introduzca su email',
        buttons: [{
          text: 'CERRAR',
          role: 'cancel'
        }]
      });
      alert.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      content: 'Enviando email, espere...'
    });
    loading.present();

    setTimeout(() => {
      this.webservice.recoverPass(value.email)
      .then(
        data => {

          loading.dismissAll();
          this.navCtrl.popToRoot();
          const alert = this.alertCtrl.create({
            title: 'Te hemos enviado un email. Revísalo para restablecer tu contraseña.'
          });
          alert.present();

        },
        err => {

          let msgerror = err.error ? err.error 
          	: 'Vaya... parece que tenemos problemas para restablecer tu contraseña. Inténtelo de nuevo más tarde.';

          loading.dismissAll();
          console.log("Error al restablecer contraseña: " + JSON.stringify(err));
          const alert = this.alertCtrl.create({
            message: msgerror,
            buttons: [{
              text: 'CERRAR',
              role: 'cancel'
            }]
          });
          alert.present();

        }
      );

    }, 300);
  };

}
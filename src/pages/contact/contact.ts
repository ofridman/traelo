import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';

import { WebService } from '../../providers/webservice';
import { UserService } from '../../providers/userservice';

@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html'
})

export class ContactPage {

    @ViewChild(Content) content: Content;

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        private webservice: WebService,
        private userService: UserService
    ) {
        
    }

    ngOnInit() {

    };

    

}
import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { RegisterPage } from '../../pages/register/register';
import { RecoverPage } from '../../pages/recover/recover';

import { WebService } from '../../providers/webservice';
import { UserService } from '../../providers/userservice';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  formLogin: FormGroup;

  constructor(
    public navCtrl: NavController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public formBuilder: FormBuilder,
    private webservice: WebService,
    private userService: UserService,
    private inAppBrowser: InAppBrowser
  ) {
    this.formLogin = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      accept: [false, Validators.required]
    });
  }

  register() {
    this.navCtrl.push(RegisterPage, {}, {
        animate: true,
        animation: 'transition',
        direction: 'forward'
    });
  };

  submitFacebook() {
    alert('Acceso Facebook!!');
  };

  submitLogin() {
    let value = this.formLogin.value;

    if(!value.accept){
      const alert = this.alertCtrl.create({
        message: 'Por favor, acepte la política de privacidad antes de acceder',
        buttons: [{
          text: 'CERRAR',
          role: 'cancel'
        }]
      });
      alert.present();
      return;
    }

    const loading = this.loadingCtrl.create({
      content: 'Espere...'
    });
    loading.present();
    setTimeout(() => {
      this.webservice.login(value.email, value.password)
      .then(
        data => {
          loading.dismissAll();
          this.userService.setUser(data);
          localStorage.setItem('UserId', data.id);
          localStorage.setItem('api_token', data.api_token);
          this.navCtrl.popToRoot();
          const alert = this.alertCtrl.create({
            title: '¡Bienvenido de nuevo ' + data.name + '!'
          });
          alert.present();
        },
        err => {
          loading.dismissAll();
          console.log("Error al hacer login: " + JSON.stringify(err));
          const alert = this.alertCtrl.create({
            message: 'Login incorrecto: usuario o contraseña no válido',
            buttons: [{
              text: 'OK',
              role: 'cancel'
            }]
          });
          alert.present();
        }
      );

      // .then((data: any) => {
      //   if(data.success){
      //     localStorage.setItem('UserId', data.id);
      //     localStorage.setItem('UserName', data.name + ' ' + data.surnames);
      //     localStorage.setItem('UserFistLetter', data.firstletter);
      //     localStorage.setItem('UserEmail', data.email);
      //     localStorage.setItem('UserAvatar', data.avatar);
      //     this.navCtrl.popToRoot();
      //     const alert = this.alertCtrl.create({
      //       title: '¡Bienvenido de nuevo, ' + data.name + '!'
      //     });
      //     alert.present();
      //   }else{
      //     const alert = this.alertCtrl.create({
      //       message: 'Los datos de acceso no pertenece a ningún usuario registrado con nosotros',
      //       buttons: [{
      //         text: 'OK',
      //         role: 'cancel'
      //       }]
      //     });
      //     alert.present();
      //   }
      //   loading.dismissAll();
      // });

    }, 1000);
  };

  recover() {
    this.navCtrl.push(RecoverPage);
  };

  openUrlPrivacidad() {
    let browser = this.inAppBrowser.create("http://traelo.es/pages/politicas.html",'_blank', {location: 'no'});
  };

  openUrlCondiciones() {
    let browser = this.inAppBrowser.create("http://traelo.es/pages/condiciones.html",'_blank', {location: 'no'});
  };

}
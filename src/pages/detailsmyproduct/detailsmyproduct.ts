import { Component } from '@angular/core';
import { Platform, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { WebService } from '../../providers/webservice';
import { AddProductPage } from '../../pages/addProduct/addProduct';
import { EditProductPage } from '../../pages/editproduct/editproduct';
import { ProfilePage } from '../../pages/profile/profile';

@Component({
  selector: 'page-details-my-product',
  templateUrl: 'details-myproduct.html'
})

export class DetailsMyProductPage {

  private urlUploadImages: string;
  private urlUploadAvatars: string;
  private idproduct: any;
  private iduser: any;
  private product: any;

  productCodeNumber: string;
  productIdUser: any;
  productUserTokenId: string;
  productUserName: string;
  productUserSurnames: string;
  productUserFirstLetter: string;
  productUserCity: string;
  productUserOnline: string;
  productUserAvatar: string;
  productTitle: string;
  productDescription: string;
  productKg: number;
  productPrice: string;
  productSure: string;
  productSold: string;
  productIsTraelo: string;
  productViews: number;
  productDateAdd: string;
  productImageOne: string;
  productImageTwo: string;
  productImageThree: string;
  productImageFour: string;
  productImageFive: string;
  productImageSix: string;
  defaultOfferInput: string;

  isLoading: boolean = true;
  isShowSlides: boolean = false;

  renovar: boolean = false;
  diffDays: number;
  daysTo: number = 0;

  parentPage: any;

  constructor(
    platform: Platform,
    public navCtrl: NavController,
    private navParams: NavParams,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private webservice: WebService
  ) {

    this.urlUploadImages = webservice.getUrlProductImages(); //'http://traelo.es/_WebService/upload/products';
    this.urlUploadAvatars = webservice.getUrlAvatarImages(); //'http://traelo.es/_WebService/upload/avatars';
    this.iduser = localStorage.getItem('UserId');
    this.idproduct = this.navParams.get('idproduct');
    this.parentPage = this.navParams.get('parentPage'); //Para refrescar la pantalla que llamó a esta

    const NO_PHOTO = 'assets/img/nophoto.png';
    
    setTimeout(() => {
      this.webservice.getProductDetails(this.idproduct)
      .then((data: any) => {

        if(!data || !data.user){
          let alert = this.alertCtrl.create({
            subTitle: 'Parece que este producto ya no se encuentra...',
            buttons: [{
              text: 'CERRAR',
              role: 'cancel'
            }],
            enableBackdropDismiss: false,
          });
          alert.present();
          return;
        }

        this.product = data;

        console.log("Pantalla detalle, producto: " + data.title);
        this.productCodeNumber = data.codenumber;
        this.productTitle = data.title;
        this.productDescription = data.description;
        this.productKg = data.kg;
        this.productPrice = data.price;
        this.productSure = data.sure;
        this.productSold = data.sold;
        this.productIsTraelo = data.istraelo;
        this.productViews = data.views;
        this.productDateAdd = data.created_at;
        this.productIdUser = data.user_id;
        this.productUserTokenId = data.usertokenid;
        if(data.user){
          this.productUserName = data.user.name;
          this.productUserSurnames = data.user.surnames;
          this.productUserFirstLetter = data.user.surnames ? data.user.surnames.substr(1, 1) : '';
          this.productUserCity = data.user.city;
          this.productUserOnline = data.user.online;
          if(data.user.avatar){
            this.productUserAvatar = this.urlUploadAvatars + '/' + this.productIdUser + '/' + data.user.avatar;
          }else{
            this.productUserAvatar = 'assets/icon/no-avatar.png';
          }
        }

        //Imagenes:
        if(data.img1){
          this.productImageOne = this.urlUploadImages + '/' + data.id + '/' + data.img1;
        }else{
          this.productImageOne = NO_PHOTO;
        }
        if(data.img2){
          this.productImageTwo = this.urlUploadImages + '/' + data.id + '/' + data.img2;
        }else{
          this.productImageTwo = NO_PHOTO;
        }
        if(data.img3){
          this.productImageThree = this.urlUploadImages + '/' + data.id + '/' + data.img3;
        }else{
          this.productImageThree = NO_PHOTO;
        }
        if(data.img4){
          this.productImageFour = this.urlUploadImages + '/' + data.id + '/' + data.img4;
        }else{
          this.productImageFour = NO_PHOTO;
        }
        if(data.img5){
          this.productImageFive = this.urlUploadImages + '/' + data.id + '/' + data.img5;
        }else{
          this.productImageFive = NO_PHOTO;
        }
        if(data.img6){
          this.productImageSix = this.urlUploadImages + '/' + data.id + '/' + data.img6;
        }else{
          this.productImageSix = NO_PHOTO;
        }

        this.defaultOfferInput = this.productPrice;
        this.isLoading = false;

        //Controles de caducidad
        this.renovar = this.isRenew(this.product);
        if(this.renovar){
          this.daysTo = 40 - this.diffDays;
        }

      });
    }, 100);
  }

  showSlides() {
    if(this.productImageTwo || this.productImageThree || this.productImageFour || this.productImageFive || this.productImageSix){
      this.isShowSlides = true;
    }
  };

  closeSlides() {
    console.log("closeSlides");
    this.isShowSlides = false;
  };

  editProduct() {
    console.log("WOOOOO voy a editproduct");
    this.navCtrl.push(EditProductPage, {
      idproduct: this.idproduct
    });
  };

  updateSold() {
    let alert = this.alertCtrl.create({
      subTitle: '¿Seguro que quiere marcar como vendido?',
      buttons: [{
        text: 'NO, SEGUIR VENDIENDO',
        role: 'cancel'
      }, {
        text: '¡SI, YA LO HE VENDIDO!',
        handler: () => {
          //Actualizamos el campo de vendido
          this.product.sold = 'yes';
          this.webservice.updateProduct(this.product).then((data: any) => {
            this.productSold = 'yes';
            //Actualizar la ventana padre:
            if(this.parentPage){
              this.parentPage.loadUserData(this.iduser).then(()=>{ console.log("Padre actualizado") });
            }
          });
        }
      }],
      enableBackdropDismiss: false,
    });
    alert.present();
  };

  //Hay que renovar?
  isRenew(product){
      var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
      var date = product.updated_at ? new Date(product.updated_at) :
        product.created_at ? new Date(product.created_at) : null;
      if(!date) return false;

      var updated = new Date(date);
      var today = new Date();

      this.diffDays = Math.round(Math.abs((today.getTime() - updated.getTime())/(oneDay)));
      return this.diffDays >= 30;
  };

  //Renovar
  renew() {
      let loading = this.loadingCtrl.create({
          content: 'Renovando producto, espere...'
      });
      loading.present();

      setTimeout(() => {

          this.webservice.renewProduct(this.product.id)
          .then((data: any) => {
      
              console.log("Resultado renovar producto OK: " + JSON.stringify(data));

              //Actualizar la ventana padre:
              if(this.parentPage){
                this.parentPage.loadUserData(this.iduser).then(()=>{ console.log("Padre actualizado") });
              }

              loading.dismiss();
              let alert = this.alertCtrl.create({
                  title: 'El producto ha sido renovado.',
                  subTitle: 'Recuerda que si no lo vendes en 40 días tendrás que volver a renovarlo para que siga en venta.',
                  buttons: [{
                      text: 'CERRAR',
                      handler: () => {
                          this.navCtrl.pop();
                      }
                  }]
              });
              alert.present();
              
          }, (err: any) => {//Error al guardar
              
              let msgError = err.msgerror ? err.msgerror:'Parece que no se ha podido renovar el producto. Inténtelo de nuevo más tarde';
              loading.dismiss();
              let alert = this.alertCtrl.create({
                    title: 'Oh oh...',
                    subTitle: msgError,
                    buttons: [{
                        text: 'ENTENDIDO',
                        role: 'cancel'
                    }]
              });
              alert.present();
              return;
          });//Fin guardar

      }, 1500);
      
  };

}
import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { NavController, NavParams } from 'ionic-angular';

import { DetailsPage } from '../../pages/details/details';
import { WebService } from '../../providers/webservice';

import { UserService } from '../../providers/userservice';

@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html'
})

export class ProfilePage {

    @ViewChild(Content) content: Content;

    public username: string;
    public city: string;
    public sellerType; string;
    public productsList: any;
    private urlUploadImages: string;
    private urlUploadAvatars: string;
    private userid: any;
    public user: any;
    userAvatar: string;
    segmentUserProfile: string;
    contentFullscreen: string = 'true';

    public valorations: any;

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        private webservice: WebService,
        private userService: UserService
    ) {
        this.segmentUserProfile = 'sales';

        this.urlUploadImages = webservice.getUrlProductImages();
        this.urlUploadAvatars = webservice.getUrlAvatarImages(); //'http://traelo.es/_WebService/_webservice/upload/avatars';
        this.userAvatar = 'assets/icon/no-avatar.png';
        this.userid = this.navParams.get('userid');
        this.user = {
            products: []
        };

        this.valorations = [];
    }

    ngOnInit() {
        //Cargar datos del usuario:
        this.loadUserData(this.userid);

        this.loadValorations(this.userid);
    };

    loadUserData(userid) {
        if(!userid) return null;
        return new Promise((resolve) => {
            setTimeout(() => {
                this.webservice.getUser(userid).then((data) => {
                  //console.log("Cargar mis datos: " + JSON.stringify(data));
                  if(data){
                    this.refreshUser(data);
                  }
                  resolve(data);
                });//fin obtener mis datos
            }, 200);
        });
    };

    loadValorations(userid) {
        if(!userid) return null;

        return new Promise((resolve) => {
            setTimeout(() => {
                this.webservice.getValorationsUser(userid).then((data) => {
                  console.log("Cargar mis datos: " + JSON.stringify(data));
                  if(data){
                    this.valorations = data;
                  }
                  resolve(data);
                });//fin obtener mis datos
            }, 200);
        });
    };

    //Actualiza los datos del usuario logado
    refreshUser(user) {
        if(!user){
            this.user = user;
            this.username = '';
            this.userAvatar = 'assets/icon/no-avatar.png';
            this.city = '';
        }else{
            this.user = user;
            //this.user.products = user.products;
            this.username = user.name + ' ' + user.surnames;
            this.userAvatar = user.avatar ? this.urlUploadAvatars + '/' + user.id + '/' + user.avatar : 'assets/icon/no-avatar.png'; 
            this.city = user.city;
            this.sellerType = user.type == 2 ? 'Vendedor profesional' : 'Vendedor particular';

            this.valorations = user.valorations || [];
        }
    };

    viewDetails(id) {
        this.navCtrl.push(DetailsPage, {
          idproduct: id,
          parentPage: this
        });
    };

    getProductsNoSold(){
        return this.user.products.filter((item) => {
            return item.sold == 'no';
        });
    }

    getProductsSold(){
        return this.user.products.filter((item) => {
            return item.sold == 'yes';
        });
    }

    getImgProduct(product){
        if(!product.img1) 
            return 'assets/img/nophoto.png';
        return this.urlUploadImages+'/'+product.id+'/'+product.img1;
    }

    //Valoraciones
    countPositives(){
        if(!this.valorations)
            return 0;

        let votes = this.valorations.filter((item) => {
            return item.rating > 0;
        });
        return votes.length;
    };
    countNegatives(){
        if(!this.valorations)
            return 0;

        let votes = this.valorations.filter((item) => {
            return item.rating < 0;
        });
        return votes.length;
    };
    getAction(valoration){
        let action = valoration.action == 'C' ? 'Le vendió ' : 'Le compró ';
        return action + valoration.product.title;
    };

    onScroll(event) {
        this.contentFullscreen = 'false';
    };

}
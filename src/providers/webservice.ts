import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import { UserService } from '../providers/userservice';

@Injectable()

export class WebService {
    private accessToken: string;
    private urlWebservice: string;
    private urlWebservice2: string;
    private urlUpload: string;
    //Nueva api:
    private urlApi: string; 
    private urlProductImages: string;
    private urlAvatarImages: string;


    public constructor(
        public http: Http,
        public userService: UserService
    ) {
        
        this.accessToken = 'C0xYX74hJrIRRQLao8LR195oNXV6lo1x';
        
        //const URL_DESA = 'http://192.168.1.34/_proyectos/apps/traelo/_webservice/';
        const URL_PROD = 'http://traelo.es/_WebService/';
        let urlBase = URL_PROD;

        this.urlWebservice = urlBase + 'traelo.php?access_token=' + this.accessToken + '&';
        this.urlWebservice2 = urlBase + 'traelo.php?access_token=' + this.accessToken;
        this.urlUpload = urlBase + 'upload/upload.php?access_token=' + this.accessToken + '&';

        this.urlProductImages = urlBase + 'upload/upload/product';
        this.urlAvatarImages = urlBase + 'upload/upload/avatars';

        //Nueva api: TODO: quitar 'public'
        this.urlApi = 'http://traelo.es/public/api';
    }

    public getUrlUpload(){
        return this.urlUpload;
    }

    public getUrlProductImages(){
        return this.urlProductImages;
    }

    public getUrlAvatarImages() {
        return this.urlAvatarImages;
    }

    //Recibe los datos del usuario logado
    public getMyUser(iduser) {

        let options = this.getAuthOptions();

        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/users/me', options)
            .map((res) => res.json())
            .subscribe((result) => {
                resolve(result);
            });
        });
    }

    //Actualiza los datos del usuario logado:
    public updateMyUser(userData) {
        return new Promise((resolve, reject) => {
            this.http.post(this.urlApi + '/users/me', userData)
            .map((res) => res.json())
            .catch(this.handleError) //Convierte el error en json
            .subscribe((result) => {
                    resolve(result);
                },(err) => {
                    reject(err);
                }
            );
        });
    }

    //Actualiza el token id del usuario para notificaciones push:
    public updatePushToken(data){
        let options = this.getAuthOptions();
        return new Promise((resolve, reject) => {
            this.http.post(this.urlApi + '/users/pushtoken', data, options)
            .map((res) => res.json())
            .catch(this.handleError) //Convierte el error en json
            .subscribe((result) => {
                    resolve(result);
                },(err) => {
                    reject(err);
                }
            );
        });
    }

    //Recibe los datos de un usuario buscando por id
    public getUser(iduser) {
        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/users/' + iduser)
            .map((res) => res.json())
            .subscribe((result) => {
                resolve(result);
            });
        });
    }

    public getToken(iduser, tokenid) {
        return new Promise((resolve) => {
            var params = 'type=getToken&iu=' + iduser + '&tk=' + tokenid;
            this.http.get(this.urlWebservice + params).map((res) => res.json()).subscribe((result) => {
              resolve(result);
            });
        });
    }

    public login(email, password) {
        var data = {
            email: email,
            password: password,
            reg_token: this.userService.getPushToken() //enviamos token push para asociarlo al usuario
        };
        return this.http.post(this.urlApi + '/users/login', data).map(res => res.json()).toPromise();
    }

    public register(user) {
        console.log("WOO json usuario a registrar: " + JSON.stringify(user));
        return new Promise((resolve, reject) => {
            this.http.post(this.urlApi + '/users', user)
            .map((res) => res.json())
            .catch(this.handleError) //Convierte el error en json
            .subscribe((result) => {
                    resolve(result);
                },(err) => {
                    reject(err);
                }
            );
        });
    }

    public recoverPass(email) {
        var data = {
            email: email
        };
        return new Promise((resolve, reject) => {
            this.http.post(this.urlApi + '/users/recoverpass', data)
            .map((res) => res.json())
            .catch(this.handleError) //Convierte el error en json
            .subscribe((result) => {
                    resolve(result);
                },(err) => {
                    reject(err);
                }
            );
        });
    }

    public changePass(oldpass, newpass) {
        var data = {
            oldpass: oldpass,
            newpass: newpass
        };
        let options = this.getAuthOptions();
        return new Promise((resolve, reject) => {
            this.http.post(this.urlApi + '/users/changepass', data, options)
            .map((res) => res.json())
            .catch(this.handleError) //Convierte el error en json
            .subscribe((result) => {
                    resolve(result);
                },(err) => {
                    reject(err);
                }
            );
        });
    }

    public getProductsList() {
        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/products')
            .map((res) => res.json())
            .subscribe((result) => {
                //console.log("Lista productos: " + JSON.stringify(result));
                resolve(result.data);
            });
        });
    }

    public addProduct(iduser, title, description, kg, price, 
    isimage1, isimage2, isimage3, isimage4, isimage5, isimage6, category, status, seguro) {
        return new Promise((resolve) => {
            //var params = 'type=addProduct&i=' + iduser + '&t=' + title + '&d=' + description + '&kg=' + kg + '&p=' + price + '&imgtwo=' + isimagetwo + '&imgthree=' + isimagethree + '&imgfour=' + isimagefour + '&imgfive=' + isimagefive + '&imgsix=' + isimagesix;
            //console.log("WOOO addProduct url: " + this.urlWebservice + params);
            var producto = {
                user_id: iduser,
                title: title,
                description: description,
                kg: kg,
                price: price,
                sure: 'no',
                sold: 'no',
                istraelo: 'no',
                views: 0,
                img1: isimage1 ? 'img1.jpg' : null,
                img2: isimage2 ? 'img2.jpg' : null,
                img3: isimage3 ? 'img3.jpg' : null,
                img4: isimage4 ? 'img4.jpg' : null,
                img5: isimage5 ? 'img5.jpg' : null,
                img6: isimage6 ? 'img6.jpg' : null,
                category: category,
                status: status,
                seguro: seguro
            };
            //console.log("WOO json producto a insertar: " + JSON.stringify(producto));

            this.http.post(this.urlApi + '/products', producto)
            .map((res) => res.json())
            .subscribe((result) => {
                console.log("WOO resultado insert producto: " + JSON.stringify(result));
                resolve(result);
            });
        });
    }

    public updateProduct(product) {
        return new Promise((resolve) => {
            //console.log("WOO json producto update: " + JSON.stringify(product));
            this.http.put(this.urlApi + '/products/'+product.id, product)
            .map((res) => res.json())
            .subscribe((result) => {
                console.log("WOO resultado update producto: " + JSON.stringify(result));
                resolve(result);
            });
        });
    }

    public renewProduct(id) {
        let options = this.getAuthOptions();

        return new Promise((resolve, reject) => {
            this.http.get(this.urlApi + '/products/renew/'+id, options)
            .map((res) => res.json())
            .catch(this.handleError) //Convierte el error en json
            .subscribe((result) => {
                    resolve(result);
                },(err) => {
                    reject(err);
                }
            );
        });
    }

    public getProductDetails(idproduct) {

        let options = this.getAuthOptions();

        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/products/' + idproduct, options)
            .map((res) => res.json())
            .subscribe((result) => {
                //console.log("Producto detalle: " + JSON.stringify(result));
                resolve(result);
            });
        });
    }

    public sendOffer(idproduct, idbuyer, iduser, offer) {
        return new Promise((resolve) => {
            var params = 'type=sendOffer&ip=' + idproduct + '&ib=' + idbuyer + '&iu=' + iduser + '&o=' + offer;
            this.http.get(this.urlWebservice + params).map((res) => res.json()).subscribe((result) => {
                resolve(result);
            });
        });
    }

    public acceptOfferOffer(id) {
        return new Promise((resolve) => {
            var params = 'type=acceptOffer&i=' + id;
            this.http.get(this.urlWebservice + params).map((res) => res.json()).subscribe((result) => {
                resolve(result);
            });
        });
    }

    public getUserConversations(iduser) {
        return this.http.get(this.urlApi + '/conversations/me/'+ iduser).map(res => res.json()).toPromise();
    }

    public removeUserConversation(id, idcreator) {
        return new Promise((resolve) => {
            var params = 'type=removeUserConversation&i=' + id + "&c=" + idcreator;
            this.http.get(this.urlWebservice + params).map((res) => res.json()).subscribe((result) => {
                resolve(result);
            });
        });
    }

    public findConversation(productid, userid) {
        let params = {
            productid: productid,
            userid: userid
        };
        return this.http.post(this.urlApi + '/conversations/find', params).map(res => res.json()).toPromise();
    }

    public createConversation(objConversation) {
        return this.http.post(this.urlApi + '/conversations', objConversation).map(res => res.json()).toPromise();
    }

    public getConversationMessages(idconversation, iduser) {
        return this.http.get(this.urlApi + '/messages/conversation/'+ idconversation).map(res => res.json()).toPromise();
    }

    public sendMessage(objMessage) {
        return this.http.post(this.urlApi + '/messages', objMessage).map(res => res.json()).toPromise();
    }

    public unreadMessages(iduser) {
        let options = this.getAuthOptions();

        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/messages/unread/'+iduser, options)
            .map((res) => res.json())
            .subscribe((result) => {
                resolve(result);
            });
        });
    }

    //MArcar mensajes como leidos
    public readMessagesConversation(idConversation) {
        let options = this.getAuthOptions();

        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/messages/read/' + idConversation, options)
            .map((res) => res.json())
            .subscribe((result) => {
                console.log("Resultado readMessagesConversation: " + JSON.stringify(result));
                resolve(result);
            });
        });
    }

    public sendPush(idpush, title, message, tokenid) {
        return new Promise((resolve) => {
            var params = 'type=sendPush&i=' + idpush + '&t=' + title + '&m=' + message + '&m=' + message + '&to=' + tokenid;
            this.http.get(this.urlWebservice + params).map((res) => res.json()).subscribe((result) => {
                resolve(result);
            });
        });
    }

    public productSearch(data) {
        return this.http.post(this.urlApi + '/products/search', data).map(res => res.json()).toPromise();
    }


    public getShippingCosts(productid) {
        let options = this.getAuthOptions();
        return this.http.get(this.urlApi + '/products/shippingcosts/'+productid, options)
        .map(res => res.json()).toPromise();
    }

    //Valoraciones del usuario logado:
    public getValorations() {
        let options = this.getAuthOptions();

        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/valorations', options)
            .map((res) => res.json())
            .subscribe((result) => {
                resolve(result);
            });
        });
    }

    //Valoraciones de un usuario distinto al logado:
    public getValorationsUser(iduser) {
        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/valorations/'+iduser)
            .map((res) => res.json())
            .subscribe((result) => {
                resolve(result);
            });
        });
    }

    //Valoraciones pendientes de hacer por el usuario logado. (Aunque se envíe el id de usuario, se usa el token)
    public getPendingValorations(iduser) {
        let options = this.getAuthOptions();

        return new Promise((resolve) => {
            this.http.get(this.urlApi + '/valorations/pending/'+iduser, options)
            .map((res) => res.json())
            .subscribe((result) => {
                resolve(result);
            });
        });
    }

    public updateValoration(valoration) { //TODO: enviar cabecera de id de usuario para dar seguridad
        return new Promise((resolve) => {
            this.http.put(this.urlApi + '/valorations/'+valoration.id, valoration)
            .map((res) => res.json())
            .subscribe((result) => {
                console.log("WOO resultado update valoration: " + JSON.stringify(result));
                resolve(result);
            });
        });
    }


    //Metodo para extraer errores del servidor
    private handleError(err: Response) {
        let json = err.text();
        let objError = JSON.parse(json.toString());
        return Observable.throw(objError?objError:err);  
    }

    //Crea cabecera para authenticación por token
    private getAuthOptions() {
        let api_token = this.userService.getApiToken();
        if(!api_token)
            return null;

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', 'Bearer ' + api_token);
        //console.log("token: " + api_token);
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    //****** Tablas maestras *********

    //Devuelve lista de comunidades autónomas
    public getRegions() {
        return this.http.get(this.urlApi + '/regions').map(res => res.json()).toPromise();
    }

    //Devuelve lista de provincias
    public getProvinces() {
        return this.http.get(this.urlApi + '/provinces').map(res => res.json()).toPromise();
    }

    //Devuelve lista de provincias
    public getCategories() {
        return this.http.get(this.urlApi + '/categories').map(res => res.json()).toPromise();
    }

    //Devuelve lista de pesos
    public getPesos() {
        return this.http.get(this.urlApi + '/pesos').map(res => res.json()).toPromise();
    }

}
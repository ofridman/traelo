import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { WebService } from '../../providers/webservice';
import { UserService } from '../../providers/userservice';

@Component({
    selector: 'page-ratinguser',
    templateUrl: 'ratinguser.html'
})

export class RatingUserPage {

    @ViewChild(Content) content: Content;

    public userLogged: any;

    public username: string;
    public city: string;
    private urlUploadImages: string;
    private urlUploadAvatars: string;
    private userid: any;
    public user: any;
    userAvatar: string;
    contentFullscreen: string = 'true';

    public formRating: FormGroup;
    public rating: any;
    public valoration: any;

    constructor(
        public navCtrl: NavController,
        private navParams: NavParams,
        private webservice: WebService,
        private userService: UserService,
        public formBuilder: FormBuilder,
        private alertCtrl: AlertController,
        public loadingCtrl: LoadingController,
    ) {
        this.formRating = this.formBuilder.group({
            comments: [''],
        });

        //usuario logado:
        this.userLogged = this.userService.getUser();

        //usuario que está siendo valorado:
        this.urlUploadImages = webservice.getUrlProductImages();
        this.urlUploadAvatars = webservice.getUrlAvatarImages(); //'http://traelo.es/_WebService/_webservice/upload/avatars';
        this.userAvatar = 'assets/icon/no-avatar.png';
        
        this.valoration = this.navParams.get('valoration');
        this.refreshUser(this.valoration.user); //Usuario valorado
    }

    ngOnInit() {
        //Cargar datos del usuario:
        //this.loadUserData(this.userid);
    };

    //Actualiza los datos del usuario logado
    refreshUser(user) {
        if(!user){
            this.user = user;
            this.username = '';
            this.userAvatar = 'assets/icon/no-avatar.png';
            this.city = '';
        }else{

            this.user = user;
            this.username = user.name + ' ' + user.surnames;
            this.userAvatar = user.avatar ? 
                this.urlUploadAvatars + '/' + user.id + '/' + user.avatar : 'assets/icon/no-avatar.png'; 
            this.city = user.city;
        }
    };

    toggleRating(result){
        this.rating = result;
    };

    getAction(valoration){
        let action = valoration.action == 'C' ? 'Le vendiste ' : 'Le compraste ';
        return action + valoration.product.title;
    }

    sendRating() {

        if(!this.rating){
            let alert = this.alertCtrl.create({
                title: 'Atención',
                subTitle: 'Por favor indica si tu valoración es positiva o negativa',
                buttons: [{
                    text: 'Cerrar',
                    role: 'cancel'
                }]
            });
            alert.present();
            return;
        }

        let value = this.formRating.value;
        //Asignar valores que han podido cambiar
        this.valoration.comments = value.comments;
        this.valoration.rating = this.rating;
        this.valoration.completed = 1;

        let loading = this.loadingCtrl.create({
            content: 'Registrando valoración, espere...'
        });
        loading.present();

        setTimeout(() => {
            this.webservice.updateValoration(this.valoration).then((dataUpdate: any) => {
                console.log("WOOOO update ok id: " + dataUpdate.id);
                loading.dismiss();

                if(!dataUpdate.id){
                    let alert = this.alertCtrl.create({
                        title: 'Oh oh...',
                        subTitle: 'Parece que no se ha podido registrar tu valoración. Inténtelo de nuevo más tarde.',
                        buttons: [{
                            text: 'ENTENDIDO',
                            role: 'cancel'
                        }]
                    });
                    alert.present();
                    return;
                }

                let alert = this.alertCtrl.create({
                    title: '¡Gracias!',
                    subTitle: 'Tu valoración ha sido registrada.',
                    buttons: [{
                        text: 'Cerrar',
                        role: 'cancel',
                        handler: () => {
                            this.navCtrl.popToRoot();
                        }
                    }]
                });
                alert.present();

                
            });//Fin guardar
        }, 2000);
        
    };

    onScroll(event) {
        this.contentFullscreen = 'false';
    };

}
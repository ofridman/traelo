import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

import { WebService } from '../../providers/webservice';
import { UserService } from '../../providers/userservice';
import { DetailsPage } from '../../pages/details/details';

@Component({
  selector: 'page-results',
  templateUrl: 'results.html'
})

export class ResultsPage {

  public productsList: any;
  private urlUploadImages: string;
  private urlUploadAvatars: string;

  userLogged: any;
  subscription: Subscription;
  message: any;

  moreScroll: number = 10;

  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    public loadingCtrl: LoadingController,
    private webservice: WebService,
    private userService: UserService
  ) {
    this.urlUploadImages = webservice.getUrlProductImages();
    this.urlUploadAvatars = webservice.getUrlAvatarImages();
    this.userLogged = this.userService.getUser();

    this.productsList = this.navParams.get('productsList');
  };

  ngOnInit() {
  };

  viewDetails(id) {
    this.navCtrl.push(DetailsPage, {
      idproduct: id
    });
  };

  goHome() {
    this.navCtrl.popToRoot();
  };

}